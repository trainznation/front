<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * BlogController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = "Blog";


        return view('blog.index', compact('page_title'));
    }

    public function show($blog_id)
    {
        try {
            $blog = $this->trainznation->get('/blog/'.$blog_id)->object();
        }catch (Exception $exception) {
            return abort(500, $exception->getMessage());
        }

        $page_title = $blog->data->title;

        $cols = collect($blog->data->comments)->sortDesc();

        return view('blog.show', compact('page_title'), [
            "blog" => $blog->data,
            "comments" => (object) $cols->values()->all()
        ]);
    }
}
