<?php

namespace App\Http\Controllers\Download;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * DownloadController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $categories = $this->trainznation->get('/download/category/list')->object();
            $assets = $this->trainznation->get('/download/list')->object();
        }catch (Exception $exception) {
            return $exception->getMessage();
        }

        $page_title = "Téléchargement";

        return view('download.index', compact('page_title'), [
            "categories" => $categories->data,
            "assets" => $assets->data
        ]);
    }

    public function show($download_id)
    {
        try {
            $asset = $this->trainznation->get('/download/'.$download_id)->object();
        }catch (Exception $exception) {
            return $exception->getMessage();
        }

        $page_title = "Téléchargement";
        $page_description = $asset->data->designation;

        return view('download.show', compact('page_title', 'page_description'), [
            "asset" => $asset->data
        ]);
    }
}
