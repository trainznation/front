<?php

namespace App\Http\Controllers\Route;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * RouteController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = "Route";

        return view('route.index', compact('page_title'));
    }

    public function show($route_id)
    {
        try {
            $route = $this->trainznation->get('/route/'.$route_id)->object();
        }catch (Exception $exception) {
            return $exception->getMessage();
        }
        $page_title = "Route";
        $page_description = $route->data->name;

        $latest_version = collect($route->data->versions);
        $latest_download = collect($route->data->downloads);
        $versions = collect($route->data->versions)->sortBy('published_at', SORT_DESC);

        $downloads = collect($route->data->downloads)->where('published', 1)->sortKeysDesc(SORT_DESC);
        $sessions = collect($route->data->sessions)->where('published', 1);

        //dd($download_first);

        return view('route.show', compact('page_title', 'page_description'), [
            "route" => $route->data,
            "latest_version" => $latest_version->last(),
            "latest_download" => $latest_download->where('published', 1)->sortBy('published_at', SORT_DESC)->last(),
            "versions" => $versions,
            "downloads" => $downloads,
            "sessions" => $sessions
        ]);
    }

    public static function count_task_card($card, $field)
    {
        $collect = collect($card->tasks);

        switch ($field)
        {
            case 'total': return $collect->count(); break;
            case 'finish': return $collect->where('status', 'Terminer')->count();
            case 'percent':
                if($collect->count() !== 0) {
                    return round((100*$collect->where('status', 'Terminer')->count())/$collect->count(), 0);
                } else {
                    return 0;
                }
            default: return null;
        }
    }
}
