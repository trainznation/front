<?php

namespace App\Http\Controllers\Tutoriel;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class TutorielController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * TutorielController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $categories = $this->trainznation->get('/tutoriel/category/list')->object();
            $tutoriels = $this->trainznation->get('/tutoriel/list')->object();
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        $page_title = "Tutoriel";

        return view('tutoriel.index', compact('page_title', 'categories', 'tutoriels'));
    }

    public function show($tutoriel_id)
    {
        try {
            $tutoriel = $this->trainznation->get('/tutoriel/'.$tutoriel_id)->object();
        }catch (Exception $exception) {
            return $exception->getMessage();
        }

        $page_title = "Tutoriel";
        $page_description = $tutoriel->data->title;


        return view('tutoriel.show', compact('page_title', 'page_description'), [
            "tutoriel" => $tutoriel->data
        ]);
    }
}
