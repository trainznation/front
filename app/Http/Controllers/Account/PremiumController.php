<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Stripe\StripeClient;

class PremiumController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * PremiumController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = "Tarification de l'offre PREMIUM";

        return view('account.premium.index', compact('page_title'));
    }

    public function subscribe()
    {
        $page_title = "Souscription";

        return view('account.premium.subscribe', compact('page_title'));
    }

    public function desactivate()
    {
        $stripe = new StripeClient(config('stripe.secret_key'));
        try {
            $subscription = $stripe->subscriptions->cancel(\request('sub'));
            $this->trainznation->put('/auth/premium/desactivate', []);
        }catch (Exception $exception) {
            return back()->with('error', "Impossible d'annuler l'abonnement, contacter un administrateur");
        }

        return back()->with('success', "Votre abonnement à été annulée et prendra fin le <strong>".Carbon::createFromTimestamp($subscription->canceled_at)->format('d/m/Y')."</strong>");
    }
}
