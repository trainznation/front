<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Stripe\StripeClient;

class OrderController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * OrderController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $stripe = new StripeClient(config('stripe.secret_key'));
        try {
            $orders = $this->trainznation->get('/auth/order')->object();
            $invoices = $stripe->invoices->all(["customer" => session()->get('user')->account->customer_id]);
        }catch (Exception $exception) {
            return $exception;
        }

        $page_title = "Liste de mes commandes";
        //dd($orders);

        return view('account.order.index', compact('page_title'), [
            "orders" => $orders->data,
            "invoices" => $invoices->data
        ]);
    }

    public function show($order_id)
    {
        try {
            $order = $this->trainznation->get('/auth/order/'.$order_id)->object();
        }catch (Exception $exception) {
            return $exception;
        }

        $page_title = "Commande N°".$order->data->reference;

        //dd($order->data);

        return view('account.order.show', compact('page_title'), [
            "order" => $order->data
        ]);
    }
}
