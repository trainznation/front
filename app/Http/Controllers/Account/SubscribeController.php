<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Stripe\StripeClient;

class SubscribeController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SubscribeController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function subscribe(Request $request)
    {
        //dd($request->all());
        $stripe = new StripeClient(config('stripe.secret_key'));

        if($request->has('stripeCard') == false) {
            try {
                $stripe->customers->createSource($request->get('customer_id'), ["source" => $request->get('stripeToken')]);
            }catch (Exception $exception) {
                Log::error($exception->getMessage());
                return back()->with('error', "L'enregistrement de votre carte bancaire à échoué, contacter un administrateur");
            }
        }

        try {
            $stripe->subscriptions->create([
                "customer" => $request->get('customer_id'),
                "items" => [
                    ["price" => $request->get('plan')]
                ]
            ]);
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return back()->with('error', "La souscription à échoué, contacter un administrateur");
        }

        try {
            $this->trainznation->put('/auth/premium/activate', [
                "plan" => $request->get('plan')
            ]);
        }catch (Exception $exception) {
            return back()->with('error', "La souscription à échoué, contacter un administrateur");
        }
        return redirect()->route('login')->with('success', "La souscription est maintenant effective, nous vous remercions !");
    }
}
