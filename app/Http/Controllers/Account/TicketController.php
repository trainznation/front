<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * TicketController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function store(Request $request)
    {
        try {
            $ticket = $this->trainznation->post('/support/ticket', [
                "subject" => $request->get('subject'),
                "message" => $request->get('message'),
                "support_category_id" => $request->get('support_category_id'),
                "support_sector_id" => $request->get('support_sector_id'),
                "support_ticket_source_id" => $request->get('support_ticket_source_id'),
                "requester_id" => session()->get('user')->id
            ])->status();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        dd($ticket);
        return response()->json(null);
    }
}
