<?php

namespace App\Http\Controllers\Account;

use App\Helpers\StripeClass;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Stevebauman\Location\Facades\Location;
use Stripe\StripeClient;

class ProfilController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var StripeClient
     */
    private $stripe;

    /**
     * ProfilController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
        if (!session()->has('user')) {
            return redirect()->route('home');
        }
    }

    public function profil()
    {
        $page_title = "Mon Profil";
        try {
            $user = $this->trainznation->post('/auth/me', [])->object();
        }catch (Exception $exception) {
            return redirect()->route('login')->with('error', "Vous avez été deconnecter du service (sessionTimeout)");
        }

        if (env('APP_ENV') == 'local' || $user->account->last_ip == '192.168.1.254') {
            $location = (object)[
                "countryName" => "France",
                "regionName" => "Pays de la Loire",
                "cityName" => "Les Sables d'Olonne"
            ];
        } else {
            $location = (object)Location::get(session()->get('user')->account->last_ip);
        }

        //dd(Carbon::createFromTimestamp($this->getListSubscription()->current_period_end)->format('d/m/Y'));

        return view('account.profil', compact('page_title'), [
            "user" => $user,
            "location" => $location,
            "cards" => StripeClass::getListCards(),
            "invoices" => StripeClass::getListInvoice(),
            "orders" => $user->orders,
            "subscription" => StripeClass::getListSubscription()
        ]);
    }

    public function editing(Request $request)
    {
        try {
            $this->trainznation->put('/auth/editing', [
                "name" => $request->get('name'),
                "email" => $request->get('email'),
                "site_web" => $request->get('site_web')
            ]);
        } catch (Exception $exception) {
            return response()->json(null, 500);
        }

        return response()->json(null, 200);
    }

    public function delete()
    {
        try {
            $this->trainznation->delete('/auth/delete');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return back()->with('error', "Erreur Système, contacter un administrateur !");
        }

        return redirect()->route('logout')->with('success', 'Votre compte à été supprimer avec succès');
    }

    public function changePassword(Request $request)
    {
        try {
            $request->validate([
                "password" => "required|min:6"
            ]);
        }catch (ValidationException $exception) {
            return back()->with('error', $exception->getMessage());
        }

        try {
            $this->trainznation->put('/auth/changePassword', [
                "password" => $request->get('password')
            ]);
        }catch (Exception $exception) {
            return back()->with('error', $exception->getMessage());
        }

        return back()->with('success', 'Mot de passe changer');
    }
}
