<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * SocialController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function SocialProviding($provider)
    {
        try {
            $social = $this->trainznation->get('/auth/socialite/'.$provider)->body();
        }catch (Exception $exception) {
            dd($exception->getMessage());
        }

        return redirect()->away($social);
    }

}
