<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * DownloadController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $lists = $this->trainznation->get('/auth/download')->object();
        }catch (Exception $exception) {
            return abort(500, $exception->getMessage());
        }

        $page_title = "Liste de vos téléchargements disponible";

        return view('account.download.index', compact('page_title'), [
            "downloads" => $lists->data
        ]);
    }
}
