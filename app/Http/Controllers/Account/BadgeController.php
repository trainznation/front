<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Illuminate\Http\Request;

class BadgeController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * BadgeController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = "Mes Badges";

        return view('account.badge.index', compact('page_title'), [
            "badges" => $this->trainznation->post('/auth/me', [])->object()->achievements
        ]);
    }
}
