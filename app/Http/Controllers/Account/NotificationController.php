<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * NotificationController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $notifs = $this->trainznation->get('/notification', ["user_id" => session()->get('user')->id])->object();
        }catch (Exception $exception) {
            return abort(500, $exception->getMessage());
        }

        $page_title = "Mes Notifications";

        return view('account.notification.index', compact('page_title'), [
            "notifications" => $notifs->data
        ]);
    }

    public function read($notification_id)
    {
        try {
            $this->trainznation->get('/auth/notification/'.$notification_id.'/read');
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json(null, 500);
        }

        return response()->json(null, 200);
    }

    public function readAll()
    {
        try {
            $this->trainznation->get('/auth/notification/readAll');
        }catch (Exception $exception) {
            Log::error($exception->getMessage());
            return response()->json(null, 500);
        }

        return response()->json(null, 200);
    }
}
