<?php

namespace App\Http\Controllers\Cart;

use App\Helpers\Calculator;
use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Darryldecode\Cart\Cart;
use Darryldecode\Cart\Facades\CartFacade;
use Exception;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CartController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        $page_title = "Mon Panier";

        return view('cart.index', compact('page_title'), [
            "content" => CartFacade::getContent(),
            "total" => CartFacade::getTotal()
        ]);
    }

    public function store(Request $request)
    {
        if(!session()->has('user'))
            return redirect()->route('login')
                ->with('warning', "Vous devez être connecter afin de pouvoir ajouter un objet dans le panier. Si vous n'avez pas de compte, il vous suffit d'en créer un !");
        try {
            $asset = $this->trainznation->get('/download/'.$request->get('id'))->object();
            $cart = CartFacade::add([
                'id' => $asset->data->id,
                'name' => $asset->data->designation,
                'price' => (session()->get('user')->account->premium_percent_shop !== null) ? Calculator::percentValue($asset->data->price, session()->get('user')->account->premium_percent_shop) : $asset->data->price,
                'quantity' => 1,
                'attributes' => [],
                'associatedModel' => $asset->data,
            ]);
        }catch (Exception $exception) {
            return back()->with('error', "Erreur Système, veuillez contacter un administrateur");
        }
        //dd(CartFacade::getContent());
        return back()->with('cart', 'ok');
    }

    public function destroy($id)
    {
        CartFacade::destroy($id);

        return redirect()->back();
    }
}
