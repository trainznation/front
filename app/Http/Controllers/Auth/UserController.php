<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Packages\Api\Auth;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * UserController constructor.
     * @param Trainznation $trainznation
     * @param Auth $auth
     */
    public function __construct(Trainznation $trainznation, Auth $auth)
    {
        $this->trainznation = $trainznation;
        $this->auth = $auth;
    }

    public function me()
    {
        return response()->json(Auth::Mee()->object());
    }

    public function readNotif($user_id, $notif_id)
    {
        try {
            $this->trainznation->get("/admin/user/".$user_id."/notifications/".$notif_id."/read");
        }catch (Exception $exception) {
            return response()->json(["error" => $exception], 500);
        }

        return response()->json(null, 200);
    }
}
