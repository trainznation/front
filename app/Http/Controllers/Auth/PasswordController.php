<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Packages\Api\Auth;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PasswordController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * PasswordController constructor.
     * @param Trainznation $trainznation
     * @param Auth $auth
     */
    public function __construct(Trainznation $trainznation, Auth $auth)
    {
        $this->trainznation = $trainznation;
        $this->auth = $auth;
    }

    public function resetRequest()
    {
        return view('auth.password.request');
    }

    public function postResetRequest(Request $request)
    {
        try {
            $search = $this->trainznation->post('/auth/search', [
                'type' => 'email',
                'email' => $request->get('email')
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json(null, 500);
        }

        dd($search->body());
    }

    public function checkToken($token)
    {
        return view('auth.password.check', ['token' => $token]);
    }

}
