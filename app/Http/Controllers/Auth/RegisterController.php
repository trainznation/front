<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Packages\Api\Auth;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Stripe\Stripe;
use Stripe\StripeClient;

class RegisterController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * RegisterController constructor.
     * @param Trainznation $trainznation
     * @param Auth $auth
     */
    public function __construct(Trainznation $trainznation, Auth $auth)
    {
        $this->trainznation = $trainznation;
        $this->auth = $auth;
    }

    public function index()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        try {
            $customer_id = $this->createStripeCustomer($request->get('email'), $request->get('name'))->id;
            $this->auth->registerUser($request->get('name'), $request->get('email'), $request->get('password'), $customer_id);
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json(null, 500);
        }

        return response()->json(null, 200);
    }

    private function createStripeCustomer($email, $name)
    {
        $stripe = new StripeClient(config('stripe.secret_key'));

        try {
            $customers = $stripe->customers->create([
                "email" => $email,
                "name" => $name
            ]);
        }catch (Exception $exception) {
            return $exception;
        }

        return $customers;
    }
}
