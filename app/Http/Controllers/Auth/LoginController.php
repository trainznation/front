<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Packages\Api\Auth;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;
    /**
     * @var Auth
     */
    private $auth;

    /**
     * LoginController constructor.
     * @param Trainznation $trainznation
     * @param Auth $auth
     */
    public function __construct(Trainznation $trainznation, Auth $auth)
    {
        $this->trainznation = $trainznation;
        $this->auth = $auth;
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        try {
            $login = $this->trainznation->authUser($request->get('email'), $request->get('password'))->object();
            session()->put('api_token', $login->access_token);

            try {
                $me = $this->auth->me()->object();
                session()->put('user', $me);
            }catch (Exception $exception) {
                Log::error($exception);
                return response()->json(null, 900);
            }
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json(null, 401);
        }

        return response()->json(null, 200);
    }

    public function logout()
    {
        try {
            $this->auth->logout();
            session()->flush();
        }catch (Exception $exception) {
            Log::error($exception);
            return back()->with('error', "Erreur lors de la déconnexion, veuillez contacter un administrateur (trainznation@gmail.com)");
        }

        return redirect()->route('home');
    }
}
