<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * PaymentController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function store(Request $request)
    {
        try {
            $this->trainznation->post('/checkout/'.$request->get('order_id').'/payment', [
                "payment_id" => $request->get('payment_id')
            ]);
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json(null, 200);
    }
}
