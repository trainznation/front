<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Darryldecode\Cart\Facades\CartFacade;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Stripe\Checkout\Session;
use Stripe\PaymentIntent;
use Stripe\Stripe;
use Stripe\StripeClient;

class CheckoutController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CheckoutController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function create()
    {
        $page_title = "Nouvelle commande";
        $carts = CartFacade::getContent();
        $total = CartFacade::getTotal();

        return view('checkout.create', compact('page_title', 'carts', 'total'));
    }

    public function store(Request $request)
    {
        $items = CartFacade::getContent();

        try {
            $order = $this->trainznation->post('/checkout/create', [
                "reference" => strtoupper(Str::random(8)),
                "total" => CartFacade::getTotal(),
                "user_id" => \session()->get('user')->id
            ])->object();
        } catch (Exception $exception) {
            Log::error($exception);
            return response()->json($exception);
        }


        foreach ($items as $item) {
            try {
                $this->trainznation->post('/checkout/' . $order->data->id . '/product', [
                    "name" => $item->name,
                    "total_price_gross" => $item->price,
                    "quantity" => $item->quantity,
                    "asset_id" => $item->associatedModel->id
                ]);
            } catch (Exception $exception) {
                Log::error($exception);
                return response()->json($exception);
            }
        }
        CartFacade::clear();

        return redirect()->route('Checkout.payment')->with('order', $order);
    }

    public function payment()
    {
        if(\session()->has('order') == true) {
            $order = session()->get('order')->data;
        } else {
            try {
                $order = $this->trainznation->get('/auth/order/'.\request('order_id'))->object()->data;
            }catch (Exception $exception) {
                return back()->with('error', "Erreur Système, contacter un administrateur");
            }
        }

        $page_title = "Paiement de la commande N°" . $order->reference;

        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));
        try {
            $intent = PaymentIntent::create([
                "amount" => $order->total * 100,
                "currency" => 'eur',
                "metadata" => [
                    "order" => $order->reference
                ],
                "payment_method_types" => ['card'],
                "receipt_email" => \session()->get('user')->email
            ]);
        }catch (Exception $exception) {
            Log::error($exception);
            return $exception;
        }

        return view('checkout.paiement', compact('page_title', 'order'), [
            "client_secret" => $intent->client_secret
        ]);
    }


}
