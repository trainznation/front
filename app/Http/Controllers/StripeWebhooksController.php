<?php

namespace App\Http\Controllers;

use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StripeWebhooksController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * StripeWebhooksController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function __invoke()
    {
        if(\request('type') === 'charge.succeeded') {
            try {
                $this->trainznation->post('/notification', [
                    "admin" => false,
                    "customer_id" => \request('data')['object']['customer'],
                    "title" => "Paiement Réussi",
                    "status" => "success",
                    "description" => "Votre paiement à été accepter par stripe",
                    "icon" => "fas fa-check"
                ]);
            }catch (Exception $exception) {
                Log::error($exception);
            }

            return "Charged";
        }
    }
}
