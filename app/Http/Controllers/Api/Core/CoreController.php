<?php

namespace App\Http\Controllers\Api\Core;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CoreController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * CoreController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function listAnnouncement()
    {
        try {
            $lists = $this->trainznation->get('/core/announcement/list')->object();
        }catch (Exception $exception) {
            Log::error($exception);
            return response()->json(null, 500);
        }

        return response()->json($lists->data, 200);
    }
}
