<?php

namespace App\Http\Controllers\Api\Route;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class RouteController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * RouteController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function list()
    {
        try {
            $routes = $this->trainznation->get('/route/list')->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($routes->data);
    }

    public function getGallery($route_id, $category_id)
    {
        try {
            $gallerie = $this->trainznation->get('/route/'.$route_id.'/gallery/category/'.$category_id)->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($gallerie->data);
    }
}
