<?php

namespace App\Http\Controllers\Api\Asset;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * AssetController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function latest()
    {
        try {
            $latests = $this->trainznation->get('/assets/latest')->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($latests, 200);
    }
}
