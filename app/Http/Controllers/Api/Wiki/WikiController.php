<?php

namespace App\Http\Controllers\Api\Wiki;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class WikiController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * WikiController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function search(Request $request)
    {
        try {
            $lists = $this->trainznation->post('/wiki/search', ['q' => $request->get('q')])->object()->data;
        }catch (Exception $exception) {
            return response()->json($exception->getMessage());
        }

        return response()->json($lists);
    }
}
