<?php

namespace App\Http\Controllers\Api\Blog;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * BlogController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function latest()
    {
        try {
            $lists = $this->trainznation->get('/blog/latest')->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($lists, 200);
    }

    public function list()
    {
        try {
            $lists = $this->trainznation->get('/blog/list')->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($lists, 200);
    }

    public function postComment(Request $request, $blog_id)
    {
        try {
            $comment = $this->trainznation->post('/blog/'.$blog_id.'/comment', [
                "user_id" => $request->get('user_id'),
                "comment" => $request->get('comment')
            ])->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($comment, 200);
    }

    public function deleteComment($blog_id, $comment_id)
    {
        try {
            $this->trainznation->delete('/blog/'.$blog_id.'/comment/'.$comment_id)->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json(null, 200);
    }
}
