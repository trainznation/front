<?php

namespace App\Http\Controllers\Api\Tutoriel;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;

class TutorielController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * TutorielController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function latest()
    {
        try {
            $latest = $this->trainznation->get('/tutoriel/latest')->object();
        }catch (Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($latest, 200);
    }
}
