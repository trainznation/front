<?php

namespace App\Http\Controllers\Wiki;

use App\Http\Controllers\Controller;
use App\Packages\Api\Trainznation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WikiController extends Controller
{
    /**
     * @var Trainznation
     */
    private $trainznation;

    /**
     * WikiController constructor.
     * @param Trainznation $trainznation
     */
    public function __construct(Trainznation $trainznation)
    {
        $this->trainznation = $trainznation;
    }

    public function index()
    {
        try {
            $categories = $this->trainznation->get('/wiki/category/list')->object()->data;
        }catch (Exception $exception) {
            Log::error($exception);
            return back()->with('error', "Erreur lors de l'affichage de la page. Veuillez contacter un administrateur");
        }

        $page_title = "Wiki";

        return view('wiki.index', compact('page_title', 'categories'));
    }

    public function sector($category_id)
    {
        try {
            $category = $this->trainznation->get('/wiki/sector/'.$category_id)->object()->data;
        }catch (Exception $exception) {
            Log::error($exception);
            return back()->with('error', "Erreur lors de l'affichage de la page. Veuillez contacter un administrateur");
        }

        $page_title = "Wiki";

        return view('wiki.sector', compact('page_title', 'category'));
    }

    public function articles($sector_id)
    {
        try {
            $sector = $this->trainznation->get('/wiki/sector/'.$sector_id.'/lists')->object()->data;
        }catch (Exception $exception) {
            Log::error($exception);
            return back()->with('error', "Erreur lors de l'affichage de la page. Veuillez contacter un administrateur");
        }

        $page_title = "Wiki";
        $page_description = "Secteur: ".$sector->name;

        return view('wiki.articles', compact('page_title', 'page_description', 'sector'));
    }

    public function show($article_id)
    {
        try {
            $article = $this->trainznation->get('/wiki/'.$article_id)->object()->data;
        }catch (Exception $exception) {
            Log::error($exception);
            return back()->with('error', "Erreur lors de l'affichage de la page. Veuillez contacter un administrateur");
        }

        $page_title = "Wiki";

        return view('wiki.show', compact('page_title', 'article'));
    }
}
