<?php


namespace App\Helpers;


class Calculator
{
    public static function percentValue($value, $percent)
    {
        $calc = ($value * $percent) / 100;
        return round(9.99 - $calc, 2);
    }

    public static function numberFormat($value, $euro = true)
    {
        if($euro == true) {
            return number_format($value, 2, ',', ' ')." €";
        } else {
            return number_format($value, 2, ',', ' ');
        }
    }
}
