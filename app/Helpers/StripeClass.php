<?php


namespace App\Helpers;


use Stripe\StripeClient;

class StripeClass
{
    public static function getListCards()
    {
        $stripe = new StripeClient(config('stripe.secret_key'));
        try {
            $cards = $stripe->customers->allSources(session()->get('user')->account->customer_id, ["object" => "card"]);
        }catch (Exception $exception) {
            return $exception;
        }

        return $cards->data;
    }

    public static function getListInvoice()
    {
        $stripe = new StripeClient(config('stripe.secret_key'));
        try {
            $invoices = $stripe->invoices->all([
                "customer" => session()->get('user')->account->customer_id
            ]);
        }catch (Exception $exception) {
            return $exception;
        }

        return $invoices->data;
    }

    public static function getListSubscription()
    {
        $stripe = new StripeClient(config('stripe.secret_key'));
        try {
            $subscriptions = $stripe->subscriptions->all(["customer" => session()->get('user')->account->customer_id]);
        }catch (Exception $exception) {
            return $exception;
        }

        if(isset($subscriptions->data[0])) {
            return $subscriptions->data[0];
        } else {
            return null;
        }
    }
}
