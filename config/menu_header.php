<?php
// Header menu
return [

    'items' => [
        [],
        [
            'title' => 'Accueil',
            'root' => true,
            'page' => '/',
            'new-tab' => false,
            'icon' => 'fas fa-home'
        ],
        [
            'title' => 'News',
            'root' => true,
            'page' => '/blog'
        ],
        [
            'title' => 'Routes',
            'root' => true,
            'page' => '/route'
        ],
        [
            'title' => 'Téléchargement',
            'root' => true,
            'page' => '/download'
        ],
        [
            'title' => 'Tutoriel',
            'root' => true,
            'page' => '/tutoriel'
        ],
        [
            'title' => 'Wiki',
            'root' => true,
            'page' => '/wiki'
        ],
    ]

];
