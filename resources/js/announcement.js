function checkAnnouncement() {
    $.ajax({
        url: '/api/core/announcement/list',
        success: (data) => {
            let div = document.querySelector('#announcement')
            div.innerHTML = '';

            Array.from(data).forEach((item) => {
                let type = {
                    'info': {'class': 'info', 'icon': 'flaticon2-information'},
                    'danger': {'class': 'danger', 'icon': 'flaticon-warning'},
                    'warning': {'class': 'warning', 'icon': 'flaticon-warning'},
                    'success': {'class': 'success', 'icon': 'flaticon2-check-mark'},
                }
                div.innerHTML += `
                    <div class="alert alert-custom alert-${type[item.type].class} mb-2" role="alert">
                        <div class="alert-icon"><i class="${type[item.type].icon}"></i></div>
                        <div class="alert-text">${item.message}</div>
                    </div>
                `
            })
        }
    })
}

checkAnnouncement();
