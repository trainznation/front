const marked = require('markdown').markdown
const Countdown = require('ds-countdown')
let content = KTUtil.getById('tutorielContent')

content.innerHTML = marked.toHTML(content.dataset.content)

let countdown = KTUtil.getById('countdown')

if(countdown) {
    new Countdown({
        id: 'countdown',
        targetTime: countdown.dataset.time,
        noDay: false,
        hideDayAtZero: false,
        separator: ':',
    })
}
