const marked = require('markdown').markdown

let content = KTUtil.getById('content_description')

content.innerHTML = marked.toHTML(content.dataset.content)

function appearMeshes() {
    let iframe = KTUtil.getById('api-frame')
    let uid = iframe.dataset.uid

    let client = new Sketchfab(iframe)

    client.init(uid, {
        success: function onSuccess(api) {
            api.start()
            api.addEventListener( 'viewerready', function() {

                // API is ready to use
                // Insert your code here
                console.log( 'Viewer is ready' );

            } );
        },
        error: function onError() {
            console.log( 'Viewer error' );
        }
    })
}

$("#btnDownload").on('click', (e) => {
    e.preventDefault()
    let btn = document.querySelector("#btnDownload")
    let uri = btn.dataset.asset;

    window.location.href=uri;
})

appearMeshes()
