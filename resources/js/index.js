$("#slider1").revolution({
    sliderType: "standard",
    sliderLayout:"fullwidth",
    delay:9000,
    navigation: {
        arrows:{enable:true}
    },
    gridwidth:1230,
    gridheight:720
})

let now = moment().format("YYYY-MM-DD hh:mm:ss");
let vnow = moment().add('1', "d").format("YYYY-MM-DD hh:mm:ss");

function newsLoader() {
    let div = document.querySelector('#newsLoader')

    KTApp.block(div, {
        overlayColor: '#000000',
        state: 'warning', // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg
    })

    $.ajax({
        url: '/api/blog/latest',
        success: (data) => {
            div.innerHTML = '';
            Array.from(data.data).forEach((item) => {
                let social = {
                    0: {'class': '', 'icon': 'fab fa-facebook', 'text': 'Non publier sur les reseaux sociaux'},
                    1: {'class': 'text-success', 'icon': 'fab fa-facebook', 'text': 'Publier sur les reseaux sociaux'},
                }
                div.innerHTML += `
                    <div class="item">
                        <div class="card card-custom gutter-b">
                             <div class="card-header bgi-no-repeat bgi-position-center ribbon ribbon-top ribbon-ver" style="background-image: url(${item.images}); height: 320px;">
                                 <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                    ${(item.published_at.format < vnow && item.published_at.format >= now) ? item.published_at.human : item.published_at.normalize}
                                 </div>
                             </div>
                             <div class="card-body">
                                 <div class="mb-3"><a href="/blog/${item.id}" class="font-weight-bold font-size-h3 text-dark">${item.title}</a></div>
                                  ${item.short_content}
                             </div>
                             <div class="card-footer row">
                                <div class="col-md-6 text-muted">
                                    <i class="fas fa-comments"></i> ${Array.from(item.comments).length}
                                </div>
                                <div class="col-md-6 text-right text-muted">
                                    <i class="${social[item.social].icon} ${social[item.social].class}" data-toggle="tooltip" title="${social[item.social].text}"></i>
                                </div>
                             </div>
                        </div>
                    </div>
                `
            })
            $('#newsLoader').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 4300,
                items: 3,
            })
        }
    })
}
function downloadLoader() {
    let div = document.querySelector('#downloadLoader')

    KTApp.block(div, {
        overlayColor: '#000000',
        state: 'warning', // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg
    })

    $.ajax({
        url: '/api/asset/latest',
        success: (data) => {
            div.innerHTML = '';
            Array.from(data.data).forEach((item) => {
                let social = {
                    0: {'class': '', 'icon': 'fab fa-facebook', 'text': 'Non publier sur les reseaux sociaux'},
                    1: {'class': 'text-success', 'icon': 'fab fa-facebook', 'text': 'Publier sur les reseaux sociaux'},
                }
                let pricing = {
                    0: {'class': 'success', 'value': 'Gratuit'},
                    1: {'class': 'danger', 'value': item.price+' €'},
                }
                let meshes = {
                    0: {'class': '', 'icon': 'fas fa-dice-d20', 'text': 'Mesh non disponible'},
                    1: {'class': 'text-success', 'icon': 'fas fa-dice-d20', 'text': 'Mesh Disponible'},
                }
                div.innerHTML += `
                    <div class="item">
                        <div class="card card-custom gutter-b">
                             <div class="card-header bgi-no-repeat bgi-position-center ribbon ribbon-top ribbon-ver" style="background-image: url(${item.image}); height: 320px;">
                                 <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                    ${(item.published_at.format < vnow && item.published_at.format >= now) ? item.published_at.human : item.published_at.normalize}
                                 </div>
                             </div>
                             <div class="card-body ribbon ribbon-clip ribbon-left">
                                 <div class="ribbon-target" style="top: 12px;">
                                    <span class="ribbon-inner bg-${pricing[item.pricing].class}"></span> ${pricing[item.pricing].value}
                                 </div>
                                 <div class="mb-3 mt-5"><a href="/download/${item.id}" class="font-weight-bold font-size-h3 text-dark">${item.designation}</a></div>
                                  ${item.short_description}
                             </div>
                             <div class="card-footer row">
                                <div class="col-md-6 text-muted">
                                    <i class="fas fa-download"></i> ${item.count_download}
                                </div>
                                <div class="col-md-6 text-right text-muted">
                                    <i class="${social[item.social].icon} ${social[item.social].class}" data-toggle="tooltip" title="${social[item.social].text}"></i>
                                    <i class="${meshes[item.meshes].icon} ${meshes[item.meshes].class}" data-toggle="tooltip" title="${meshes[item.meshes].text}"></i>
                                </div>
                             </div>
                        </div>
                    </div>
                `
            })
            $('#downloadLoader').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 4300,
                items: 3,
            })
        }
    })
}
function tutorielLoader() {
    let div = document.querySelector('#tutorielLoader')

    KTApp.block(div, {
        overlayColor: '#000000',
        state: 'warning', // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg
    })

    $.ajax({
        url: '/api/tutoriel/latest',
        success: (data) => {
            div.innerHTML = '';
            Array.from(data.data).forEach((item) => {
                let social = {
                    0: {'class': '', 'icon': 'fab fa-facebook', 'text': 'Non publier sur les reseaux sociaux'},
                    1: {'class': 'text-success', 'icon': 'fab fa-facebook', 'text': 'Publier sur les reseaux sociaux'},
                }
                let premium = {
                    0: {'class': '', 'icon': 'fas fa-certificate', 'text': 'Pour Tous'},
                    1: {'class': 'text-success', 'icon': 'fas fa-certificate', 'text': 'Uniquement pour les premium'},
                }
                let source = {
                    0: {'class': '', 'icon': 'fas fa-archive', 'text': 'Source non disponible'},
                    1: {'class': 'text-success', 'icon': 'fas fa-archive', 'text': 'Source Disponible'},
                }
                div.innerHTML += `
                    <div class="item">
                        <div class="card card-custom gutter-b">
                             <div class="card-header bgi-no-repeat bgi-position-center ribbon ribbon-top ribbon-ver" style="background-image: url(${item.image}); height: 320px;">
                                 <div class="ribbon-target bg-${(item.published_at.format > now) ? 'warning' : 'danger'}" style="top: -2px; right: 20px;">
                                    ${(item.published_at.format < vnow && item.published_at.format >= now || item.published_at.format > vnow) ? item.published_at.human : item.published_at.normalize}
                                 </div>
                             </div>
                             <div class="card-body">
                                 <div class="mb-3 mt-5"><a href="/tutoriel/${item.id}" class="font-weight-bold font-size-h3 text-dark">${item.title}</a></div>
                                  ${item.short_content}
                             </div>
                             <div class="card-footer row">
                                <div class="col-md-6 text-muted">
                                    <i class="far fa-clock"></i> ${(item.time) ? item.time : 'Non disponible'}
                                </div>
                                <div class="col-md-6 text-right text-muted">
                                    <i class="${social[item.social].icon} ${social[item.social].class}" data-toggle="tooltip" title="${social[item.social].text}"></i>
                                    <i class="${premium[item.premium].icon} ${premium[item.premium].class}" data-toggle="tooltip" title="${premium[item.premium].text}"></i>
                                    <i class="${source[item.source].icon} ${source[item.source].class}" data-toggle="tooltip" title="${source[item.source].text}"></i>
                                </div>
                             </div>
                        </div>
                    </div>
                `
            })
            $('#tutorielLoader').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 4300,
                items: 3,
            })
        }
    })
}

newsLoader()
downloadLoader()
tutorielLoader()
console.log(now)
