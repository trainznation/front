const marked = require('markdown').markdown
const slugify = require('slugify')

const Elements = {
    markedown: document.querySelector('#markedDown'),
    summary: document.querySelector('#summary'),
}

Elements.markedown.innerHTML = marked.toHTML(Elements.markedown.dataset.content)

function mapSummary() {
    let elems = Elements.markedown.querySelectorAll('h1')
    Elements.summary.innerHTML = ''
    Array.from(elems).forEach(elem => {
        console.log(elem.textContent)
        let h2s = elem.querySelectorAll('h2')
        let h2content = ''
        console.log(h2s)
        Array.from(h2s).forEach(item => {
            let slugh2 = slugify(item.textContent)
            item.setAttribute('id', slugh2)
            h2content += `
            <li class="navi-item">
                <a href="#${slugh2}" class="navi-link">${item.textContent}</a>
            </li>
            `
        })
        let slug = slugify(elem.textContent)
        Elements.summary.innerHTML += `
            <li class="navi-item">
                <a href="#${slug}" class="navi-link">${elem.textContent}</a>
                <ul class="navi navi-bold navi-hover my-5">${h2content}</ul>
            </li>
        `
        elem.setAttribute('id', slug)
    })
}

mapSummary()


