const Elements = {
    inputSearch: document.querySelector('#searchWiki'),
    result: document.querySelector('#searchResult')
}

Elements.inputSearch.addEventListener('keyup', e => {
    e.preventDefault()
    KTApp.block(Elements.result, {message: 'Chargement...'})

    $.ajax({
        url: '/api/wiki/search',
        method: 'POST',
        data: {q: Elements.inputSearch.value},
        success: (data) => {
            KTApp.unblock(Elements.result)
            console.log(data)

            let content = '';

            if(Array.from(data).length > 0) {
                Array.from(data).forEach(item => {
                    content += `
                    <div class="d-flex align-items-center flex-grow-1 mb-2">
                        <div class="symbol symbol-30 symbol-2by3 flex-shrink-0">
                            <div class="symbol-label"><i class="fas fa-book-open"></i> </div>
                        </div>
                        <div class="d-flex flex-column ml-3 mt-2 mb-2">
                            <a href="/wiki/${item.id}" class="font-weight-bold text-dark text-hover-primary">
                                ${item.title}
                            </a>
                            <span class="font-size-sm font-weight-bold text-muted">
                                ${item.category.name}<br>
                                <em>${item.sector.name}</em>
                            </span>
                        </div>
                    </div>
                `
                })
            } else {
                content += `
                    <div class="d-flex align-items-center flex-grow-1 mb-2">
                        <div class="symbol symbol-30 symbol-2by3 flex-shrink-0">
                            <div class="symbol-label"><i class="fas fa-exclamation-triangle"></i> </div>
                        </div>
                        <div class="d-flex flex-column ml-3 mt-2 mb-2">
                            <span class="font-size-sm font-weight-bold text-muted text-center">
                                Aucun résultat pour votre recherche
                            </span>
                        </div>
                    </div>
                `
            }

            Elements.result.innerHTML = `
                <div class="quick-search-result">
                    <div class="mb-10">${content}</div>
                </div>
            `;

            Elements.result.style.display = 'block'
        },
        error: (err) => {
            KTApp.unblock(Elements.result)
            console.error(err)
        }
    })
})

Elements.inputSearch.addEventListener('blur', e => {
    e.preventDefault()
    Elements.result.style.display = 'none';
    Elements.inputSearch.value = '';
})
