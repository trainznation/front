const spinning = 'spinner spinner-right spinner-white pr-15'

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#formPasswordRequest").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formPasswordRequest")
    let uri = form.attr('action')
    let btn = document.querySelector('#kt_login_singin_form_submit_button')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true)

    $.ajax({
        url: uri,
        method: 'POST',
        data: data,
        statusCode: {
            200: (data) => {
                KTUtil.btnRelease(btn)
                if(data.data === 0) {
                    toastr.warning("Cette adresse n'existe pas dans notre base de donnée !", "Adresse Introuvable")
                } else {
                    toastr.success("Un email vous à été envoyer à l'adresse renseigner, veuillez cliquer sur le lien afin de redéfinir votre mot de passe", "Réinitialisation du mot de passe")
                }
            }
        }
    })
})
