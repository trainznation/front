const spinning = 'spinner spinner-right spinner-white pr-15'

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#formRegister").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formRegister")
    let uri = form.attr('action')
    let btn = document.querySelector('#kt_login_singin_form_submit_button')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez Patienter...', true)

    $.ajax({
        url: uri,
        method: 'post',
        data: data,
        statusCode: {
            200: () => {
                KTUtil.btnRelease(btn)
                toastr.success("Votre compte à été créer")
                setTimeout(() => {
                    window.location.href='/auth/login'
                }, 1500)
            },
            500: () => {
                KTUtil.btnRelease(btn)
                toastr.error("Une ou plusieurs erreur ont eu lieu, contactez un administrateur (trainznation@gmail.com)", "Erreur 500")
            }
        }
    })
})
