const spinning = 'spinner spinner-right spinner-white pr-15'

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#formLogin").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formLogin")
    let uri = form.attr('action')
    let btn = document.querySelector('#kt_login_singin_form_submit_button')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter', true)

    $.ajax({
        url: uri,
        method: 'post',
        data: data,
        statusCode: {
            200: (data) => {
                KTUtil.btnRelease(btn)
                window.location.href = '/'
            },
            401: (data) => {
                KTUtil.btnRelease(btn)
                toastr.warning("Veuillez vérifier votre adresse mail ou votre mot de passe", "Impossible de vous identifier")
            },
            900: (data) => {
                KTUtil.btnRelease(btn)
                toastr.error("Erreur lors de la récupération de vos informations, veuillez contacter un administrateur (contact@trainznation.eu)", "Erreur de récupération")
            },
            500: (data) => {
                KTUtil.btnRelease(btn)
                toastr.error("Erreur, veuillez contacter un administrateur (contact@trainznation.eu)", "Erreur Serveur")
            }
        }
    })
})
