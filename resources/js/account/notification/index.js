const spinning = 'spinner spinner-right spinner-white pr-15'

$("#btnReadAllNotif").on('click', (e) => {
    e.preventDefault()
    let btn = document.querySelector('#btnReadAllNotif')
    let uri = $("#btnReadAllNotif").attr('href')

    KTUtil.btnWait(btn, spinning, "Veuillez Patienter...", true)

    $.ajax({
        url: uri,
        success: () => {
            KTUtil.btnRelease(btn)
            window.location.reload()
        },
        error: () => {
            KTUtil.btnRelease(btn)
            toastr.error("Erreur Système, Contacter un administrateur", "Erreur Serveur")
        }
    })
})

let btns = document.querySelectorAll('.btn-read')
Array.from(btns).forEach((btn) => {
    btn.addEventListener('click', (e) => {
        e.preventDefault()
        let uri = btn.dataset.href

        KTUtil.btnWait(btn)

        $.ajax({
            url: uri,
            success: () => {
                KTUtil.btnRelease(btn)
                setTimeout(() => {
                    window.location.reload()
                }, 2300)
            },
            error: () => {
                KTUtil.btnRelease(btn)
                toastr.error("Erreur Système, Contacter un administrateur", "Erreur Serveur")
            }
        })
    })
})
