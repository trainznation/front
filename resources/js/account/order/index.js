const spinning = 'spinner spinner-right spinner-white pr-15'
let listeOrder;

function loadListeOrder() {
    listeOrder = $("#liste_order").KTDatatable({
        data: {
            saveState: {cookie: false}
        },
        search: {
            input: $('#search_liste_order'),
            key: 'generalSearch'
        },
        columns: [
            {
                field: "Id",
                width: 80
            },
            {
                field: "updated_at",
                width: 50,
            },
            {
                field: "total",
                width: 100
            },
            {
                field: "Status",
                width: 80,
                template: (row) => {
                    let status = {
                        0: {'text': 'Non Payé', 'class': 'danger'},
                        1: {'text': 'Payé', 'class': 'success'},
                    }

                    return `<span class="label label-inline label-pill label-${status[row.Status].class}">${status[row.Status].text}</span>`
                }
            },
            {
                field: 'Actions',
                template: (row) => {
                    return `
                        <a href="/account/order/${row.Id}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" title="Voir la commande"><i class="fas fa-eye"></i> </a>
                    `
                }
            }
        ]
    })
    $("#search_liste_status").on('change', () => {
        table.search($('#search_liste_status').val().toLowerCase(), 'Status')
    })

    $("#search_liste_status").selectpicker();
}

loadListeOrder();
