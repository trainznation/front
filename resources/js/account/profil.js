const spinning = 'spinner spinner-right spinner-white pr-15'

$("#formEditing").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formEditing")
    let uri = form.attr('action')
    let btn = document.querySelector('#btnSubmitEditing')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true)

    $.ajax({
        url: uri,
        method: 'PUT',
        data: data,
        success: (data) => {
            KTUtil.btnRelease(btn);
            toastr.success("Vos informations ont été mis à jour, vous allez être déconnecter", "Succès")
            setTimeout(() => {
                window.location.href='/auth/logout'
            }, 1500)
        },
        error: (error) => {
            KTUtil.btnRelease(btn);
            toastr.error("Erreur Système, veuillez contacter un administrateur", "Erreur Serveur");
        }
    })
})
