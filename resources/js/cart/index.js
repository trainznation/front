const spinning = 'spinner spinner-right spinner-white pr-15'

let deletes = document.querySelectorAll('.deleteItem')
deletes.forEach((btn) => {
    btn.addEventListener('click', e => {
        KTUtil.btnWait(btn, spinning)
        e.preventDefault();
        btn.parentNode.submit()
    })
})
