const spinning = 'spinner spinner-right spinner-white pr-15'
const marked = require('markdown').markdown
let now = moment();
let content = KTUtil.getById('content')

content.innerHTML = marked.toHTML(content.dataset.content)

$("#formComment").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formComment")
    let uri = form.attr('action')
    let btn = document.querySelector('#btnSubmitComment')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning, 'Veuillez patienter...', true)

    $.ajax({
        url: uri,
        method: 'POST',
        data: data,
        statusCode: {
            200: (data) => {
                KTUtil.btnRelease(btn)
                toastr.success("Votre commentaire à été poster")
                let newC = document.querySelector('#newComment')
                newC.innerHTML += `
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="d-flex flex-column mb-5 align-items-start">
                            <div class="d-flex align-items-center mb-3">
                                <div class="symbol symbol-circle symbol-40 mr-3">
                                    <img alt="Pic" src="${data.data.user.avatar.encoded}">
                                </div>
                                <div>
                                    <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">${data.data.user.name}</a>
                                    <span class="text-muted font-size-sm">
                                        ${(data.data.updated_at.format >= now.format('00:00:00') && data.data.updated_at.format <= now.format('23:59:59')) ? data.data.updated_at.human : data.data.updated_at.normalize}
                                    </span>
                                </div>
                            </div>
                            <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left">
                                ${marked.toHTML(data.data.comment)}
                            </div>
                        </div>
                    </div>
                </div>
                `
            }
        }
    })
})

let btnDeleteComment = document.querySelectorAll('.btnDeleteComment')
Array.from(btnDeleteComment).forEach((btn) => {
    btn.addEventListener('click', (e) => {
        e.preventDefault()
        let uri = btn.dataset.href

        KTUtil.btnWait(btn, spinning, null, true)


        $.ajax({
            url: uri,
            method: 'DELETE',
            success: (data) => {
                KTUtil.btnRelease(btn)
                toastr.success("Commentaire supprimer")
                btn.parentNode.parentNode.parentNode.parentNode.parentNode.style.display = 'none'
            }
        })
    })
})
