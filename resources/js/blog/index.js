const marked = require('markdown').markdown
let now = moment().format("YYYY-MM-DD hh:mm:ss");
let vnow = moment().add('1', "d").format("YYYY-MM-DD hh:mm:ss");

function loadCarousel() {
    let carousel = document.querySelector("#newsLoader")

    KTApp.block(carousel, {
        overlayColor: '#000000',
        state: 'warning', // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg
    })

    $.ajax({
        url: '/api/blog/latest',
        success: (data) => {
            carousel.innerHTML = '';
            Array.from(data.data).forEach((item) => {
                let social = {
                    0: {'class': '', 'icon': 'fab fa-facebook', 'text': 'Non publier sur les reseaux sociaux'},
                    1: {'class': 'text-success', 'icon': 'fab fa-facebook', 'text': 'Publier sur les reseaux sociaux'},
                }

                carousel.innerHTML += `

                <div class="item">
                    <div class="card card-custom gutter-b">
                         <div class="card-header bgi-no-repeat bgi-position-center ribbon ribbon-top ribbon-ver" style="background-image: url(${item.images}); height: 320px;">
                             <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                ${(item.published_at.format < vnow && item.published_at.format >= now) ? item.published_at.human : item.published_at.normalize}
                             </div>
                         </div>
                         <div class="card-body">
                             <div class="mb-3"><a href="" class="font-weight-bold font-size-h3 text-dark">${item.title}</a></div>
                              ${item.short_content}
                         </div>
                         <div class="card-footer row">
                            <div class="col-md-6 text-muted">
                                <i class="fas fa-comments"></i> ${Array.from(item.comments).length}
                            </div>
                            <div class="col-md-6 text-right text-muted">
                                <i class="${social[item.social].icon} ${social[item.social].class}" data-toggle="tooltip" title="${social[item.social].text}"></i>
                            </div>
                         </div>
                    </div>
                </div>

                `
            })

            $('#newsLoader').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 4300,
                items: 5,
            })
        }
    })
}
function loadNews() {
    let news = document.querySelector('#loadElement')

    KTApp.block(news, {
        overlayColor: '#000000',
        state: 'warning', // a bootstrap color
        size: 'lg' //available custom sizes: sm|lg
    })

    $.ajax({
        url: '/api/blog/list',
        success: (data) => {
            news.innerHTML = '';
            Array.from(data.data).forEach((item) => {
                news.innerHTML += `
                <div class="card card-custom card-rounded mb-5 card-news" data-href="/blog/${item.id}" style="cursor: pointer">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img
                                src="${item.images}"
                                alt="" class="card-img" style="height: 100%">
                        </div>
                        <div class="col-md-8">
                            <div class="card-header">
                                <h3 class="card-title">${item.title}</h3>
                            </div>
                            <div class="card-body">
                                ${marked.toHTML(item.short_content)}
                            </div>
                            <div class="card-footer">
                                <div class="row no-gutters text-muted">
                                    <div class="col-md-6">
                                        <i class="fas fa-calendar"></i> Posté le ${item.published_at.normalize}
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <i class="fas fa-comments"></i> ${Array.from(item.comments).length}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `
            })

            let cards = document.querySelectorAll('.card-news')

            Array.from(cards).forEach((item) => {
                item.addEventListener('click', () => {
                    window.location.href=item.dataset.href;
                })
            })
        }
    })
}

function showCard() {

}
loadCarousel();
loadNews();
