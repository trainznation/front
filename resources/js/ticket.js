const spinning = 'spinner spinner-right spinner-white pr-15'
$.ajax({
    url: $("#support_category_id").attr('data-server'),
    success: (data) => {
        let content = '';
        data.data.forEach(item => {
            content += `<option value="${item.id}">${item.name}</option>`
        })
        $("#support_category_id").html(content)
    }
})

$("#support_category_id").on('change', (e) => {
    e.preventDefault()
    $.ajax({
        url: 'https://api.trainznation.tk/api/support/category/'+$("#support_category_id").val()+'/sector',
        success: (data) => {
            let content = '';
            data.data.forEach(item => {
                content += `<option value="${item.id}">${item.name}</option>`
            })
            $("#support_sector_id").html(content)
        }
    })
})

$("#formNewTicket").on('submit', (e) => {
    e.preventDefault();
    let form = $("#formNewTicket")
    let url = form.attr('action')
    let btn = document.querySelector('.chat-sending')
    let data = form.serializeArray()

    KTUtil.btnWait(btn, spinning)

    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: () => {
            KTUtil.btnRelease(btn)
            toastr.success("Votre ticket à été soumis à notre équipe. Vous pouvez suivre l'avancer de votre ticket dans votre espace > Mes Tickets", "Nouveau Ticket d'assistance")
            form[0].reset()
        },
        error: (err) => {
            KTUtil.btnRelease(btn)
            console.error(err)
            toastr.error("Une erreur à eu lieu, contacter un administrateur", "Erreur Serveur")
        }
    })
})
