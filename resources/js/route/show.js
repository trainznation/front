const marked = require('markdown').markdown

let elements = {
    apexChartGeneral: document.getElementById('apex_general_tasks_average'),
    apexChartGeneralOne: document.getElementById('apex_general_tasks_average_one'),
    contentDescription: KTUtil.getById('contentDescription'),
    navsGallery: document.querySelectorAll('.navi-gallery'),
    contentGallery: KTUtil.getById('js-content'),
}

function initApexChartTaskAverage() {
    let height = parseInt(KTUtil.css(elements.apexChartGeneral, 'height'))


    switch (elements.apexChartGeneral.dataset.percent) {
        case elements.apexChartGeneral.dataset.percent <= 33:
            elements.color = '#E53935'
            break;
        case elements.apexChartGeneral.dataset.percent >= 34 && elements.apexChartGeneral.dataset.percent <= 66:
            elements.color = '#e58735'
            break;
        case elements.apexChartGeneral.dataset.percent >= 66:
            elements.color = '#64e535'
            break;
    }

    if(!elements.apexChartGeneral) {
        return;
    }

    let options = {
        series: [elements.apexChartGeneral.dataset.percent],
        chart: {
            height: height,
            type: 'radialBar'
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    margin: 0,
                    size: "65%"
                },
                dataLabels: {
                    showOn: "always",
                    name: {
                        show: false,
                        fontWeight: '700'
                    },
                    value: {
                        color: '#BDBDBD',
                        fontSize: "30px",
                        fontWeight: '700',
                        offsetY: 12,
                        show: true
                    }
                },
                track: {
                    background: '#BDBDBD',
                    strokeWidth: elements.apexChartGeneral.dataset.percent+'%'
                }
            }
        },
        colors: ['#64e535'],
        stroke: {
            lineCap: "round",
        },
        labels: ["Progress"]
    };
    console.log(elements.color)
    let chart = new ApexCharts(elements.apexChartGeneral, options);
    chart.render();
}
function initApexChartTaskAverageOne() {
    let height = parseInt(KTUtil.css(elements.apexChartGeneralOne, 'height'))


    switch (elements.apexChartGeneralOne.dataset.percent) {
        case elements.apexChartGeneralOne.dataset.percent <= 33:
            elements.color = '#E53935'
            break;
        case elements.apexChartGeneralOne.dataset.percent >= 34 && elements.apexChartGeneralOne.dataset.percent <= 66:
            elements.color = '#e58735'
            break;
        case elements.apexChartGeneralOne.dataset.percent >= 66:
            elements.color = '#64e535'
            break;
    }

    if(!elements.apexChartGeneralOne) {
        return;
    }

    let options = {
        series: [elements.apexChartGeneralOne.dataset.percent],
        chart: {
            height: height,
            type: 'radialBar'
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    margin: 0,
                    size: "65%"
                },
                dataLabels: {
                    showOn: "always",
                    name: {
                        show: false,
                        fontWeight: '700'
                    },
                    value: {
                        color: '#BDBDBD',
                        fontSize: "30px",
                        fontWeight: '700',
                        offsetY: 12,
                        show: true
                    }
                },
                track: {
                    background: '#BDBDBD',
                    strokeWidth: elements.apexChartGeneralOne.dataset.percent+'%'
                }
            }
        },
        colors: ['#64e535'],
        stroke: {
            lineCap: "round",
        },
        labels: ["Progress"]
    };
    console.log(elements.color)
    let chart = new ApexCharts(elements.apexChartGeneralOne, options);
    chart.render();
}
Array.from(elements.navsGallery).forEach((nav) => {
    nav.addEventListener('click', (e) => {
        call(nav.dataset.route, nav.dataset.categoryid)
    })
})

function call(route, categoryid) {
    KTApp.block(elements.contentGallery, {
        overlayColor: '#000000',
        state: 'danger',
        message: 'Veuillez patienter...'
    })
    $.ajax({
        url: `/api/route/${route}/gallery/category/${categoryid}`,
        success: (data) => {
            KTApp.unblock(elements.contentGallery)
            let galleries = data.galleries
            elements.contentGallery.innerHTML = '';
            if(Array.from(galleries).length === 0) {
                elements.contentGallery.innerHTML = `
                    <div class="col-md-12 bg-gray-200 text-center">
                        <h3 class="p-7"><i class="flaticon-warning-sign icon-2x"></i> Aucune image dans cette catégorie de gallerie</h3>
                    </div>
                `
            } else {
                Array.from(galleries).forEach((gallery) => {
                    elements.contentGallery.innerHTML += `
                <div class="col-md-4">
                <div class="card card-custom overlay mb-5">
                    <div class="card-body p-0">
                        <div class="overlay-wrapper">
                            <img src="${gallery.image}" alt="" class="w-100 rounded">
                        </div>
                            <div class="overlay-layer align-items-start justify-content-end pt-5 pr-5">
                                <a href="${gallery.image}" data-lightbox="image-${gallery.category}" class="btn btn-clean btn-icon">
                                    <i class="flaticon-eye icon-lg text-primary"></i>
                                </a>
                                <a data-href="/route/${route}/gallery/${gallery.id}/delete" class="btn btn-clean btn-icon" onclick="deleteImg($(this))">
                                    <i class="flaticon2-trash icon-lg text-primary"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                `
                })
            }
        },
        error: (err) => {
            KTApp.unblock(elements.contentGallery)
            toastr.error("Erreur d'affichage de la gallerie")
            console.error(err)
        }
    })
}

elements.contentDescription.innerHTML = marked.toHTML(elements.contentDescription.dataset.content)

initApexChartTaskAverage();
initApexChartTaskAverageOne();
