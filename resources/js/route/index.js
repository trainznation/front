let routeElement = document.querySelector('#routeElement')

KTApp.block(routeElement, {
    overlayColor: '#000000',
    state: 'warning', // a bootstrap color
    size: 'lg' //available custom sizes: sm|lg
})

$.ajax({
    url: '/api/route/list',
    success: (data) => {
        routeElement.innerHTML = '';
        Array.from(data).forEach((item) => {
            routeElement.innerHTML += `
            <div class="col-md-4">
                <div class="item">
                    <div class="card card-custom gutter-b">
                        <div class="card-header bgi-no-repeat bgi-position-center ribbon ribbon-top ribbon-ver" style="background-image: url('${item.image}'); height: 320px;">
                            <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                ${item.build}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="mb-3"><a href="" class="font-weight-bold font-size-h3 text-dark">${item.name}</a></div>
                            ${item.description.substring(0,150)}...
                        </div>
                        <div class="card-footer">
                            <a href="/route/${item.id}" class="btn btn-block btn-primary">Voir la Route</a>
                        </div>
                    </div>
                </div>
            </div>
            `
        })
    }
})
