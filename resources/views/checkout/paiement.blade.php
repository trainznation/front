{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom">
        <div class="card-body text-center">
            <h3 class="font-weight-bold">Information de votre paiement</h3>
            <strong>Montant: </strong> {{ number_format($order->total, 2, ',', ' ') }} €<br>
            <strong>Identité: </strong> {{ session()->get('user')->name }}<br>
            <strong>Email: </strong> {{ session()->get('user')->email }}<br>

            <form id="payment-form">
                <div id="card-element"></div>
                <div id="card-errors" role="alert"></div>
                <input type="hidden" id="clientsecret" value="{{ $client_secret }}">
                <input type="hidden" id="order_id" value="{{ $order->id }}">
                <button id="btnSubmitStripe" class="btn btn-info">Payer par carte bancaire</button>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{ asset('js/checkout/paiement.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        let stripe = Stripe('pk_test_ABmLgBDKfEyzAMdg0cYVsLAf00itkPB2kP');
        let elements = stripe.elements()

        let style = {
            base: {
                color: "#32325d",
            }
        };

        let card = elements.create("card", { style: style });
        card.mount("#card-element");

        card.on('change', ({error}) => {
            let displayError = document.getElementById('card-errors');
            if (error) {
                displayError.textContent = error.message;
            } else {
                displayError.textContent = '';
            }
        });

        let form = document.getElementById('payment-form');
        let btn = document.getElementById('btnSubmitStripe')
        let order_id = document.getElementById('order_id')

        form.addEventListener('submit', function(ev) {
            ev.preventDefault();
            KTUtil.btnWait(btn)
            stripe.confirmCardPayment($("#clientsecret").val(), {
                payment_method: {
                    card: card,
                }
            }).then(function(result) {
                if (result.error) {
                    // Show error to your customer (e.g., insufficient funds)
                    console.log(result.error.message);
                } else {
                    // The payment has been processed!
                    if (result.paymentIntent.status === 'succeeded') {
                        $.ajax({
                            url: '/checkout/payment',
                            method: "POST",
                            data: {order_id: order_id.value, payment_id: result.paymentIntent.id},
                            success: (data) => {
                                KTUtil.btnRelease(btn)
                                toastr.success("Votre Paiement N° "+result.paymentIntent.id+" à été exécuté avec succès")
                                setTimeout(() => {
                                    window.location.href='/'
                                }, 1500)
                            },
                            error: (error) => {
                                console.log(error)
                                toastr.error("Veuillez contacter un administrateur", "Erreur Système")
                            }
                        })

                    }
                }
            });
        });
    </script>
@endsection
