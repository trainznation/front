{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom mb-5">
        <div class="card-header">
            <h3 class="card-title">Récapitulatif de la commande</h3>
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Produit</th>
                        <th>Qte</th>
                        <th>Montant</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($carts as $cart)
                    <tr>
                        <td class="d-flex align-items-center font-weight-bolder">
                            <!--begin::Symbol-->
                            <div class="symbol symbol-60 flex-shrink-0 mr-4 bg-light">
                                <div class="symbol-label" style="background-image: url('{{ $cart['associatedModel']->image }}')"></div>
                            </div>
                            <!--end::Symbol-->
                            <a href="#" class="text-dark text-hover-primary">{{ $cart['name'] }}</a>
                        </td>
                        <td class="text-center align-middle">{{ $cart['quantity'] }}</td>
                        <td class="text-right align-middle font-weight-bolder font-size-h5">{{ number_format($cart['price']*$cart['quantity'], 2, ',', ' ') }} €</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="1"></td>
                        <td class="font-weight-bolder font-size-h4 text-right">Montant à régler</td>
                        <td class="font-weight-bolder font-size-h4 text-right">{{ number_format($total, 2, ',', ' ') }} €</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="fas fa-credit-card"></i> </span>
                <h3 class="card-label">Paiement par carte bancaire</h3>
            </div>
        </div>
        <div class="card-body" id="payment-pending">
            <form id="payment-form" action="{{ route('Checkout.store') }}" method="post">
                <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-primary" style="height: 250px; background-image: url({{ asset('media/svg/patterns/taieri.svg') }})">
                    <div class="card-body d-flex">
                        <div class="d-flex py-5 flex-column align-items-start flex-grow-1">
                            <div class="flex-grow-1">
                                <p class="text-warning">Nous ne conservons aucune de ces informations sur notre site, elles sont directement transmises à notre prestataire de paiement <a href="https://stripe.com/fr">Stripe</a>.</p>
                                <p class="text-warning">La transmissiosn de ces informations est entièrement sécurisée.</p>
                            </div>
                            <button type="submit" class="btn btn-link btn-link-white font-weight-bold"><i class="fa fa-credit-card"></i> Valider votre commande et payer</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/checkout/create.js') }}" type="text/javascript"></script>
@endsection
