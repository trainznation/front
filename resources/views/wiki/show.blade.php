{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="d-flex flex-row-fluid bgi-size-cover bgi-position-top" style="background-image: url('/media/bg/bg-9.jpg')">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center pt-25 pb-35">
                <h3 class="font-weight-bolder text-dark mb-0">{{ $article->title }}</h3>
                <span class="font-size-h6 font-weight-bold text-right"><a href="{{ route('Wiki.sector', $article->category->id) }}">{{ $article->category->name }}</a>/<a href="{{ route('Wiki.articles', $article->sector->id) }}">{{ $article->sector->name }}</a></span>
            </div>
        </div>
    </div>
    <div class="container mt-n15">
        <div class="row" data-sticky-container>
            <div class="col-md-4">
                <div class="card card-custom sticky" data-sticky="true" data-margin-top="140" data-sticky-for="1023" data-sticky-class="stickyjs">
                    <div class="card-body">
                        <ul class="navi navi-bold navi-hover my-5" id="summary">
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card card-custom">
                    <div class="card-body">
                        <div id="markedDown" data-content="{{ $article->content }}"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/wiki/show.js') }}" type="text/javascript"></script>
@endsection
