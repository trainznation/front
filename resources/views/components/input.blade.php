<label for="{{ $name }}" @if($classLabel != '') class="{{ $classInput }}" @endif>{{ $label }}</label>
<input id="{{ $name }}" type="{{ $type }}" name="{{ $name }}" class="form-control {{ $classInput }}" value="{{ $value }}" required="{{ $required }}" autofocus="{{ $autofocus }}">
