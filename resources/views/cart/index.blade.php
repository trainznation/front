{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom gutter-b">
        <!--begin::Header-->
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder font-size-h3 text-dark">Mon Panier</span>
            </h3>
            <div class="card-toolbar">
                <div class="dropdown dropdown-inline">
                    <a href="{{ redirect()->back() }}" class="btn btn-primary font-weight-bolder font-size-sm">Continuer mes achats</a>
                    <a href="{{ route('Checkout.create') }}" class="btn btn-info font-weight-bolder font-size-sm">Passer commande</a>
                </div>
            </div>
        </div>
        <!--end::Header-->
        <div class="card-body">
            <!--begin::Shopping Cart-->
            <div class="table-responsive">
                <table class="table">
                    <!--begin::Cart Header-->
                    <thead>
                    <tr>
                        <th>Produit</th>
                        <th class="text-center">Qte</th>
                        <th class="text-right">Montant</th>
                        <th></th>
                    </tr>
                    </thead>
                    <!--end::Cart Header-->
                    <tbody>
                    <!--begin::Cart Content-->
                    @if($total)
                        @foreach($content as $item)
                            <tr>
                                <td class="d-flex align-items-center font-weight-bolder">
                                    <!--begin::Symbol-->
                                    <div class="symbol symbol-60 flex-shrink-0 mr-4 bg-light">
                                        <div class="symbol-label" style="background-image: url('{{ $item['associatedModel']->image }}')"></div>
                                    </div>
                                    <!--end::Symbol-->
                                    <a href="#" class="text-dark text-hover-primary">{{ $item['name'] }}</a>
                                </td>
                                <td class="text-center align-middle">
                                    <span class="mr-2 font-weight-bolder">{{ $item['quantity'] }}</span>
                                </td>
                                <td class="text-right align-middle font-weight-bolder font-size-h5">{{ number_format($item['price']*$item['quantity'], 2, ',', ' ') }} €</td>
                                <td class="text-right align-middle">
                                    <form action="{{ route('Cart.destroy', $item['id']) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button  class="btn btn-danger font-weight-bolder font-size-sm deleteItem">Supprimer</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">Le Panier est vide !</td>
                        </tr>
                    @endif
                    <!--end::Cart Content-->
                    <!--begin::Cart Footer-->
                    <tr>
                        <td colspan="2"></td>
                        <td class="font-weight-bolder font-size-h4 text-right">Total</td>
                        <td class="font-weight-bolder font-size-h4 text-right">{{ number_format($total, 2, ',', ' ') }} €</td>
                    </tr>
                    <!--end::Cart Footer-->
                    </tbody>
                </table>
            </div>
            <!--end::Shopping Cart-->
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/cart/index.js') }}" type="text/javascript"></script>
@endsection
