{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <!-- START REVOLUTION SLIDER 5.0 -->
    <div class="rev_slider_wrapper mt-n4">
        <div id="slider1" class="rev_slider"  data-version="5.0" >
            <ul>
                @foreach($slides as $slide)
                <li data-transition="fade">
                    @if($slide->link !== null)
                        <a href="{{ $slide->link }}">
                            <!-- MAIN IMAGE -->
                            <img src="{{ $slide->image }}"  alt=""  width="1920" height="1080">
                            <!-- LAYER NR. 1 -->
                        </a>
                    @else
                        <!-- MAIN IMAGE -->
                        <img src="{{ $slide->image }}"  alt=""  width="1920" height="1080">
                        <!-- LAYER NR. 1 -->
                    @endif
                </li>
                @endforeach
            </ul>
        </div><!-- END REVOLUTION SLIDER -->
    </div><!-- END OF SLIDER WRAPPER -->
    <div class="container">
        <h1 class="display-3 text-black-50 mt-5 mb-5"><i class="fas fa-newspaper display-3"></i> Dernières News</h1>

        <div class="owl-carousel owl-theme" id="newsLoader"></div>
    </div>
    <div class="divider-rounded"></div>
    <div class="container">
        <h1 class="display-3 text-black-50 mt-5 mb-5"><i class="fa fa-download display-3"></i> Derniers Téléchargement</h1>
        <div class="owl-carousel owl-theme" id="downloadLoader"></div>
    </div>
    <div class="divider-rounded"></div>
    <div class="container">
        <h1 class="display-3 text-black-50 mt-5 mb-5"><i class="fa fa-video display-3"></i> Derniers Tutoriels</h1>
        <div class="owl-carousel owl-theme" id="tutorielLoader"></div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/index.js') }}" type="text/javascript"></script>
@endsection
