<div class="quick-search-result">
    {{-- Message --}}
    <div class="text-muted d-none">
        No record found
    </div>

    {{-- Section --}}
    <div class="font-size-sm text-primary font-weight-bolder text-uppercase mb-2">
        Objets
    </div>
    <div class="mb-10">
        @foreach($assets as $asset)
            <div class="d-flex align-items-center flex-grow-1 mb-2">
                <div class="symbol symbol-30 symbol-2by3 flex-shrink-0">
                    <div class="symbol-label" style="background-image:url('{{ $asset->image }}')"></div>
                </div>
                <div class="d-flex flex-column ml-3 mt-2 mb-2">
                    <a href="{{ route('Download.show', $asset->id) }}" class="font-weight-bold text-dark text-hover-primary">
                        {{ $asset->designation }}
                    </a>
                    <span class="font-size-sm font-weight-bold text-muted">
                        {{ \Illuminate\Support\Str::limit($asset->short_description, 50, '...') }}
                    </span>
                </div>
            </div>
        @endforeach
    </div>

    {{-- Section --}}
    <div class="font-size-sm text-primary font-weight-bolder text-uppercase mb-2">
        Articles
    </div>
    <div class="mb-10">
        @foreach($blogs as $blog)
            <div class="d-flex align-items-center flex-grow-1 mb-2">
                <div class="symbol symbol-30 symbol-2by3 flex-shrink-0">
                    <div class="symbol-label" style="background-image:url('{{ $blog->images }}')"></div>
                </div>
                <div class="d-flex flex-column ml-3 mt-2 mb-2">
                    <a href="{{ route('Blog.show', $blog->id) }}" class="font-weight-bold text-dark text-hover-primary">
                        {{ $blog->title }}
                    </a>
                    <span class="font-size-sm font-weight-bold text-muted">
                        {{ \Illuminate\Support\Str::limit($blog->short_content, 50, '...') }}
                    </span>
                </div>
            </div>
        @endforeach
    </div>

    {{-- Section --}}
    <div class="font-size-sm text-primary font-weight-bolder text-uppercase mb-2">
        Tutoriels
    </div>
    <div class="mb-10">
        @foreach($tutoriels as $tutoriel)
            <div class="d-flex align-items-center flex-grow-1 mb-2">
                <div class="symbol symbol-30 symbol-2by3 flex-shrink-0">
                    <div class="symbol-label" style="background-image:url('{{ $tutoriel->image }}')"></div>
                </div>
                <div class="d-flex flex-column ml-3 mt-2 mb-2">
                    <a href="{{ route('Tutoriel.show', $tutoriel->id) }}" class="font-weight-bold text-dark text-hover-primary">
                        {{ $tutoriel->title }}
                    </a>
                    <span class="font-size-sm font-weight-bold text-muted">
                        {{ \Illuminate\Support\Str::limit($tutoriel->short_content, 50, '...') }}
                    </span>
                </div>
            </div>
        @endforeach
    </div>
</div>
