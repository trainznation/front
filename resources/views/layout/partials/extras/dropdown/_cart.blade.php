@if (config('layout.extras.cart.dropdown.style') == 'light')
    {{-- Header --}}
    <div class="d-flex align-items-center p-10 rounded-top bg-light">
        <span class="btn btn-md btn-icon bg-light-success mr-4">
            <i class="flaticon2-shopping-cart-1 text-success"></i>
        </span>
        <h4 class="flex-grow-1 m-0 mr-3">Mon Panier</h4>
        <button type="button" class="btn btn-success btn-sm">{{ $cartCount }} @if($cartCount > 1) articles @else article @endif</button>
    </div>
@else
    {{-- Header --}}
    <div class="d-flex align-items-center py-10 px-8 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url('{{ asset('media/misc/bg-1.jpg') }}')">
        <span class="btn btn-md btn-icon bg-white-o-15 mr-4">
            <i class="flaticon2-shopping-cart-1 text-success"></i>
        </span>
        <h4 class="text-white flex-grow-1 m-0 mr-3">Mon Panier</h4>
        <button type="button" class="btn btn-success btn-sm">{{ $cartCount }} @if($cartCount > 1) articles @else article @endif</button>
    </div>
@endif

{{-- Scroll --}}
<div class="scroll scroll-push" data-scroll="true" data-height="250" data-mobile-height="200">
    @foreach(\Darryldecode\Cart\Facades\CartFacade::getContent() as $cart)
    {{-- Item --}}
    <div class="d-flex align-items-center justify-content-between p-8">
        <div class="d-flex flex-column mr-2">
            <a href="{{ route('Download.show', $cart['associatedModel']->id) }}" class="font-weight-bold text-dark-75 font-size-lg text-hover-primary">
                {{ $cart['name'] }}
            </a>
            <span class="text-muted">
                {{ \Illuminate\Support\Str::limit($cart['associatedModel']->short_description, 100) }}
            </span>
            <div class="d-flex align-items-center mt-2">
                <span class="font-weight-bold mr-1 text-dark-75 font-size-lg">{{ number_format($cart['price'], 2, ',', ' ') }} € X {{ $cart['quantity'] }}</span>
            </div>
        </div>
        <a href="#" class="symbol symbol-70 flex-shrink-0">
            <img src="{{ $cart['associatedModel']->image }}" title="" alt=""/>
        </a>
    </div>

    {{-- Separator --}}
    <div class="separator separator-solid"></div>
    @endforeach
</div>

{{-- Summary --}}
<div class="p-8">
    <div class="d-flex align-items-center justify-content-between mb-4">
        <span class="font-weight-bold text-muted font-size-sm mr-2">Total</span>
        <span class="font-weight-bolder text-dark-50 text-right">{{ number_format($cartTotal, 2, ',', ' ') }} €</span>
    </div>
    <div class="text-right">
        <a href="{{ route('Cart.index') }}" class="btn btn-primary text-weight-bold">Voir mon Panier</a>
    </div>
</div>
