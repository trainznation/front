{{-- Chat --}}

<div class="modal modal-sticky modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{-- Card --}}
            <div class="card card-custom">
                {{-- Header --}}
                <div class="card-header align-items-center px-4 py-3">
                    <div class="text-left flex-grow-1">
                        {{-- Dropdown Menu  --}}
                    </div>
                    <div class="text-center flex-grow-1">
                        <div class="text-dark-75 font-weight-bold font-size-h5">Nouveau ticket d'assistance</div>
                    </div>
                    <div class="text-right flex-grow-1">
                        <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md"  data-dismiss="modal">
                            <i class="ki ki-close icon-1x"></i>
                        </button>
                    </div>
                </div>

                {{-- Body --}}
                <div class="card-body">
                    @if(session()->has('user') == true)
                        <form action="{{ route('Support.Ticket.store') }}" id="formNewTicket" method="POST">
                            <input type="hidden" name="support_ticket_source_id" value="1">
                            <div class="form-group">
                                <label>Catégorie</label>
                                <select id="support_category_id" name="support_category_id" class="form-control" required data-server="{{ env('API_ENDPOINT') }}/support/category/list"></select>
                            </div>
                            <div class="form-group">
                                <label>Secteur</label>
                                <select id="support_sector_id" name="support_sector_id" class="form-control" required></select>
                            </div>
                            <div class="form-group">
                                <label>Sujet</label>
                                <input type="text" name="subject" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control border-0 p-0" rows="6" placeholder="Tapez un message" name="message"></textarea>
                            </div>
                            <div class="d-flex align-items-center justify-content-between mt-5">
                                <div>
                                    <button type="submit" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-sending py-2 px-6">Envoyer</button>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="alert alert-custom alert-warning fade show mb-5" role="alert">
                            <div class="alert-icon">
                                <i class="flaticon-warning"></i>
                            </div>
                            <div class="alert-text">Vous devez être connecter pour ouvrir un ticket de support</div>
                            <div class="alert-close">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">
										<i class="ki ki-close"></i>
									</span>
                                </button>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
