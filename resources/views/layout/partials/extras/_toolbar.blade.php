{{-- Sticky Toolbar --}}
<ul class="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
    {{-- Item --}}
    @if (config('layout.extras.chat.display') == true)
        {{-- Item --}}
        <li class="nav-item" id="kt_sticky_toolbar_chat_toggler" data-toggle="tooltip" title="Nouveau ticket d'assistance" data-placement="left" data-step="1" data-intro="Vous pouvez ouvrir un ticket de support si vous rencontrez un souci avec nos produits">
            <a class="btn btn-sm btn-icon btn-bg-light btn-text-danger btn-hover-danger" href="#" data-toggle="modal" data-target="#kt_chat_modal">
                <i class="flaticon2-chat-1"></i>
            </a>
        </li>
    @endif
</ul>
