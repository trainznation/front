{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="owl-carousel owl-theme mb-lg-2" id="newsLoader"></div>

    <div class="container" id="loadElement">
        <div class="card card-custom card-rounded mb-5">
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img
                        src="https://images.unsplash.com/photo-1527295110-5145f6b148d0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2034&q=80"
                        alt="" class="card-img">
                </div>
                <div class="col-md-8">
                    <div class="card-header">
                        <h3 class="card-title">Titre</h3>
                    </div>
                    <div class="card-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab beatae dicta dolore fugiat harum id illum, laboriosam, nam omnis optio reprehenderit, rerum saepe? A dicta earum exercitationem iste natus!</p>
                    </div>
                    <div class="card-footer">
                        <div class="row no-gutters text-muted">
                            <div class="col-md-6">
                                <i class="fas fa-calendar"></i> Posté le 16 Octobre 2020 à 20h30
                            </div>
                            <div class="col-md-6 text-right">
                                <i class="fas fa-comments"></i> 0
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/blog/index.js') }}" type="text/javascript"></script>
@endsection
