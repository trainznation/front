{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="container">
        <div class="card card-custom">
            <div style="height: 300px; background: url('{{ $blog->images }}') center; background-size: cover; "></div>
            <div class="symbol symbol-20 symbol-lg-80 symbol-circle mr-3" style="position: relative;left: 46%;top: -40px;background-color: #fff;width: 80px;">
                <img alt="Pic" src="{{ asset('media/logos/logo-letter-1.png') }}"/>
            </div>
            <div class="card-body">
                <div class="row mb-lg-4">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div style="margin-bottom: 45px;">
                            <h2 class="card-title text-center display-4 font-weight-bold">{{ $blog->title }}</h2>
                            <div class="text-muted text-center mt-n8">{{ $blog->published_at->normalize }}</div>
                        </div>
                        <div id="content" style="font-size: 18px;" data-content="{!! $blog->content !!}"></div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="divider-rounded mb-lg-5 mt-lg-5"></div>
                        <h3 class="font-weight-bold mb-5"><i class="fas fa-comments"></i> {{ count($blog->comments) }} Commentaires</h3>
                        @if(session()->get('user'))
                            <form id="formComment" action="/api/blog/{{ $blog->id }}/comment" method="post">
                                <input type="hidden" name="user_id" value="{{ session()->get('user')->id }}">
                                <div class="form-group">
                                    <textarea class="form-control" name="comment" rows="5" placeholder="Votre commentaire" data-provide="markdown"></textarea>
                                </div>
                                <div class="text-right">
                                    <button type="submit" id="btnSubmitComment" class="btn btn-primary">Envoyer</button>
                                </div>
                            </form>
                        @else
                            <div class="alert alert-custom alert-light-primary fade show mb-5" role="alert">
                                <div class="alert-icon">
                                    <i class="flaticon-information"></i>
                                </div>
                                <div class="alert-text">Vous devez être connecter pour pouvoir poster un commentaires</div>
                            </div>
                        @endif
                        <div id="newComment"></div>
                        @foreach($comments as $comment)
                            <div class="card card-custom">
                                <div class="card-body">
                                    <div class="d-flex flex-column mb-5 align-items-start">
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="symbol symbol-circle symbol-40 mr-3">
                                                <img alt="Pic" src="{{ $comment->user->avatar->encoded }}">
                                            </div>
                                            <div>
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{ $comment->user->name }}</a>
                                                <span class="text-muted font-size-sm">
                                                    @if($comment->updated_at->format >= now()->format("00:00:00") && $comment->updated_at->format <= now()->format("23:59:59"))
                                                        {{ $comment->updated_at->human }}
                                                    @else
                                                        {{ $comment->updated_at->normalize }}
                                                    @endif
                                                </span>
                                            </div>
                                            <div>
                                                @if(session()->get('user')->id == $comment->user->id || session()->get('user')->admin == 1)
                                                    <button class="btn btn-icon btn-clean btnDeleteComment" data-href="/api/blog/{{ $blog->id }}/comment/{{ $comment->id }}"><i class="fa fa-trash text-danger"></i> </button>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left">
                                            {!! $comment->comment !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/blog/show.js') }}" type="text/javascript"></script>
@endsection
