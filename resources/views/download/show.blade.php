{{-- Extends layout --}}
@extends('layout.default')

@section('styles')

@endsection

@section('page_toolbar')
    <button class="btn btn-sm btn-clean"><i class="fas fa-arrow-circle-left"></i> Retour</button>
    @if($asset->pricing == 1)
        @if(session()->get('user'))
            @if(session()->get('user')->account->premium_percent_shop !== null)
                <form action="{{ route('Cart.store') }}" method="post">
                    <input type="hidden" name="id" value="{{ $asset->id }}">
                    <button type="submit" class="btn btn-sm btn-danger" data-toggle="tooltip" data-html="true" title="Vous bénéficier de <strong>{{ session()->get('user')->account->premium_percent_shop }}%</strong> de réduction"><i class="fas fa-shopping-cart"></i> Acheter {{ number_format(\App\Helpers\Calculator::percentValue($asset->price, session()->get('user')->account->premium_percent_shop), 2, ',', ' ')." €" }}</button>
                </form>
            @else
                <form action="{{ route('Cart.store') }}" method="post">
                    <input type="hidden" name="id" value="{{ $asset->id }}">
                    <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-shopping-cart"></i> Acheter {{ number_format($asset->price, 2, ',', ' ')." €" }}</button>
                </form>
            @endif
        @endif
    @else
        <button class="btn btn-sm btn-success" id="btnDownload" data-asset="{{ $asset->link_uuid }}"><i class="fas fa-download"></i> Télécharger</button>
    @endif
@endsection

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom">
        <div class="card-header">
            <img src="{{ $asset->image }}" alt="{{ $asset->designation }}" class="card-img" style="height: 650px;">
        </div>
        <div class="card-body">
            <ul class="nav nav-light-success nav-pills justify-content-center mb-5" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#description"><i class="fas fa-dashboard"></i> Descriptions</a>
                </li>
                @if($asset->meshes == 1)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#view"><i class="fas fa-cubes"></i> Vue 3D</a>
                    </li>
                @endif
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="description" role="tabpanel">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="card card-custom mb-3">
                                <div class="card-body">
                                    <strong>KUID:</strong> {{ '<'.$asset->kuids[0]->kuid.'>' }}<br>
                                    <strong>UUID:</strong> {{ $asset->uuid }}<br>
                                    <strong>NB de téléchargement:</strong> {{ $asset->count_download }}<br>
                                    <strong>Date de publication:</strong> {{ $asset->published_at->normalize }}
                                </div>
                            </div>
                            <div class="card card-custom mb-3">
                                <div class="card-body">
                                    <strong>Tags:</strong> @foreach($asset->tags as $tag) <span class="label label-inline bg-warning">{{ $tag->name->fr }}</span> @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="card card-custom">
                                <div class="card-body p-5" id="content_description" data-content="{{ $asset->description }}"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="view" role="tabpanel">
                    <iframe src="" data-uid="{{ $asset->sketchfab_uid }}" id="api-frame" allow="autoplay; fullscreen; vr" allowvr allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" style="width: 100%; height: 630px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
    @if(session()->has('cart'))
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-check text-success"></i> Produit ajouté au panier avec succès</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Il y à {{ $cartCount }} @if($cartCount > 1) articles @else article @endif dans votre panier pour un total de <strong>{{ number_format($cartTotal, 2, ',', ' ') }} €</strong></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Continuer mes achats</button>
                        <a href="{{ route('Cart.index') }}" class="btn btn-primary font-weight-bold">Commander</a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script type="text/javascript" src="https://static.sketchfab.com/api/sketchfab-viewer-1.8.2.js"></script>
    <script src="{{ asset('js/download/show.js') }}" type="text/javascript"></script>
    @if(session()->has('cart'))
    <script type="text/javascript">
        let modal = $("#exampleModal")
        modal.modal('show')
    </script>
    @endif
@endsection
