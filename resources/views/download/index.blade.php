{{-- Extends layout --}}
@extends('layout.default')

@section('styles')

@endsection

{{-- Content --}}
@section('content')
    @if(session()->has('user') == true)
        <div class="container-fluid">
            <div class="card card-custom">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-line justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#all">
                                <span class="nav-icon"><i class="fas fa-home"></i></span>
                                <span class="nav-text">Tous</span>
                            </a>
                        </li>
                        @foreach($categories as $category)
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#{{ $category->slug }}">
                                    <span class="nav-icon"><img src="{{ $category->image }}" alt="" style="width: 24px; height: 24px;"></span>
                                    <span class="nav-text">{{ $category->name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content mt-5">
                        <div class="tab-pane fade show active" id="all" role="tabpanel">
                            <h2 class="card-title">Liste des Téléchargements</h2>
                            <div class="row">
                                @foreach($assets as $asset)
                                    <div class="col-md-4">
                                        <a class="item" href="{{ route('Download.show', $asset->id) }}">
                                            <div class="card card-custom gutter-b">
                                                <div class="card-header bgi-no-repeat bgi-position-center bgi-size-cover ribbon ribbon-top ribbon-ver" style="background-image: url('{{ $asset->image }}'); height: 320px;">
                                                    <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                                        {{ $asset->published_at->normalize }}
                                                    </div>
                                                    @if(session()->get('user')->account->premium_percent_shop !== null && $asset->pricing == 1)
                                                        <div class="ribbon-target" style="top: 25px;">
                                                            <span class="ribbon-inner bg-warning"></span> - {{ session()->get('user')->account->premium_percent_shop }} %
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="card-body ribbon ribbon-clip ribbon-left">
                                                    <div class="ribbon-target" style="top: 12px;">
                                                        @if($asset->pricing == 1)
                                                            @if(session()->get('user')->account->premium_percent_shop !== null)
                                                                <span class="ribbon-inner bg-danger f"></span> {{ number_format(\App\Helpers\Calculator::percentValue($asset->price, session()->get('user')->account->premium_percent_shop), 2, ',', ' ')." €" }}
                                                            @else
                                                                <span class="ribbon-inner bg-danger"></span> {{ number_format($asset->price, 2, ',', ' ')." €" }}
                                                            @endif
                                                        @else
                                                            <span class="ribbon-inner bg-success"></span> Gratuit
                                                        @endif
                                                    </div>
                                                    <div class="mb-3 mt-lg-5"><a href="" class="font-weight-bold font-size-h3 text-dark">{{ $asset->designation }}</a></div>
                                                    {{ $asset->short_description }}
                                                </div>
                                                <div class="card-footer text-muted row">
                                                    <div class="col-md-6">
                                                        <i class="fas fa-download"></i> {{ $asset->count_download }}
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        @if($asset->social == 1)
                                                            <i class="socicon-facebook text-primary"></i>
                                                        @else
                                                            <i class="socicon-facebook"></i>
                                                        @endif
                                                        @if($asset->meshes == 1)
                                                            <i class="fas fa-dice-d20 text-primary"></i>
                                                        @else
                                                            <i class="fas fa-dice-d20"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @foreach($categories as $category)
                            <div class="tab-pane fade" id="{{ $category->slug }}" role="tabpanel">
                                <div class="card-title">
                                    <span class="card-icon"><img src="{{ $category->image }}" alt="" style="width: 24px; height: 24px;"/> </span>
                                    <h3 class="card-title">{{ $category->name }}</h3>
                                </div>

                                <div class="row">
                                    @foreach($category->assets as $asset)
                                        <div class="col-md-4">
                                            <a class="item" href="{{ route('Download.show', $asset->id) }}">
                                                <div class="card card-custom gutter-b">
                                                    <div class="card-header bgi-no-repeat bgi-position-center bgi-size-cover ribbon ribbon-top ribbon-ver" style="background-image: url('{{ $asset->image }}'); height: 320px;">
                                                        <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                                            {{ $asset->published_at->normalize }}
                                                        </div>
                                                        @if(session()->get('user')->account->premium_percent_shop !== null && $asset->pricing == 1)
                                                            <div class="ribbon-target" style="top: 25px;">
                                                                <span class="ribbon-inner bg-warning"></span> - {{ session()->get('user')->account->premium_percent_shop }} %
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="card-body ribbon ribbon-clip ribbon-left">
                                                        <div class="ribbon-target" style="top: 12px;">
                                                            @if($asset->pricing == 1)
                                                                @if(session()->get('user')->account->premium_percent_shop !== null)
                                                                    <span class="ribbon-inner bg-danger f"></span> {{ number_format(\App\Helpers\Calculator::percentValue($asset->price, session()->get('user')->account->premium_percent_shop), 2, ',', ' ')." €" }}
                                                                @else
                                                                    <span class="ribbon-inner bg-danger"></span> {{ number_format($asset->price, 2, ',', ' ')." €" }}
                                                                @endif
                                                            @else
                                                                <span class="ribbon-inner bg-success"></span> Gratuit
                                                            @endif
                                                        </div>
                                                        <div class="mb-3 mt-lg-5"><a href="" class="font-weight-bold font-size-h3 text-dark">{{ $asset->designation }}</a></div>
                                                        {{ $asset->short_description }}
                                                    </div>
                                                    <div class="card-footer text-muted row">
                                                        <div class="col-md-6">
                                                            <i class="fas fa-download"></i> {{ $asset->count_download }}
                                                        </div>
                                                        <div class="col-md-6 text-right">
                                                            @if($asset->social == 1)
                                                                <i class="socicon-facebook text-primary"></i>
                                                            @else
                                                                <i class="socicon-facebook"></i>
                                                            @endif
                                                            @if($asset->meshes == 1)
                                                                <i class="fas fa-dice-d20 text-primary"></i>
                                                            @else
                                                                <i class="fas fa-dice-d20"></i>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="container-fluid">
            <div class="card card-custom">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-line justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#all">
                                <span class="nav-icon"><i class="fas fa-home"></i></span>
                                <span class="nav-text">Tous</span>
                            </a>
                        </li>
                        @foreach($categories as $category)
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#{{ $category->slug }}">
                                    <span class="nav-icon"><img src="{{ $category->image }}" alt="" style="width: 24px; height: 24px;"></span>
                                    <span class="nav-text">{{ $category->name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content mt-5">
                        <div class="tab-pane fade show active" id="all" role="tabpanel">
                            <h2 class="card-title">Liste des Téléchargements</h2>
                            <div class="row">
                                @foreach($assets as $asset)
                                    <div class="col-md-4">
                                        <a class="item" href="{{ route('Download.show', $asset->id) }}">
                                            <div class="card card-custom gutter-b">
                                                <div class="card-header bgi-no-repeat bgi-position-center bgi-size-cover ribbon ribbon-top ribbon-ver" style="background-image: url('{{ $asset->image }}'); height: 320px;">
                                                    <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                                        {{ $asset->published_at->normalize }}
                                                    </div>
                                                </div>
                                                <div class="card-body ribbon ribbon-clip ribbon-left">
                                                    <div class="ribbon-target" style="top: 12px;">
                                                        @if($asset->pricing == 1)
                                                            <span class="ribbon-inner bg-danger"></span> {{ number_format($asset->price, 2, ',', ' ')." €" }}
                                                        @else
                                                            <span class="ribbon-inner bg-success"></span> Gratuit
                                                        @endif
                                                    </div>
                                                    <div class="mb-3 mt-lg-5"><a href="" class="font-weight-bold font-size-h3 text-dark">{{ $asset->designation }}</a></div>
                                                    {{ $asset->short_description }}
                                                </div>
                                                <div class="card-footer text-muted row">
                                                    <div class="col-md-6">
                                                        <i class="fas fa-download"></i> {{ $asset->count_download }}
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        @if($asset->social == 1)
                                                            <i class="socicon-facebook text-primary"></i>
                                                        @else
                                                            <i class="socicon-facebook"></i>
                                                        @endif
                                                        @if($asset->meshes == 1)
                                                            <i class="fas fa-dice-d20 text-primary"></i>
                                                        @else
                                                            <i class="fas fa-dice-d20"></i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        @foreach($categories as $category)
                            <div class="tab-pane fade" id="{{ $category->slug }}" role="tabpanel">
                                <div class="card-title">
                                    <span class="card-icon"><img src="{{ $category->image }}" alt="" style="width: 24px; height: 24px;"/> </span>
                                    <h3 class="card-title">{{ $category->name }}</h3>
                                </div>

                                <div class="row">
                                    @foreach($category->assets as $asset)
                                        <div class="col-md-4">
                                            <a class="item" href="{{ route('Download.show', $asset->id) }}">
                                                <div class="card card-custom gutter-b">
                                                    <div class="card-header bgi-no-repeat bgi-position-center bgi-size-cover ribbon ribbon-top ribbon-ver" style="background-image: url('{{ $asset->image }}'); height: 320px;">
                                                        <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                                            {{ $asset->published_at->normalize }}
                                                        </div>
                                                    </div>
                                                    <div class="card-body ribbon ribbon-clip ribbon-left">
                                                        <div class="ribbon-target" style="top: 12px;">
                                                            @if($asset->pricing == 1)
                                                                <span class="ribbon-inner bg-danger"></span> {{ number_format($asset->price, 2, ',', ' ')." €" }}
                                                            @else
                                                                <span class="ribbon-inner bg-success"></span> Gratuit
                                                            @endif
                                                        </div>
                                                        <div class="mb-3 mt-lg-5"><a href="" class="font-weight-bold font-size-h3 text-dark">{{ $asset->designation }}</a></div>
                                                        {{ $asset->short_description }}
                                                    </div>
                                                    <div class="card-footer text-muted row">
                                                        <div class="col-md-6">
                                                            <i class="fas fa-download"></i> {{ $asset->count_download }}
                                                        </div>
                                                        <div class="col-md-6 text-right">
                                                            @if($asset->social == 1)
                                                                <i class="socicon-facebook text-primary"></i>
                                                            @else
                                                                <i class="socicon-facebook"></i>
                                                            @endif
                                                            @if($asset->meshes == 1)
                                                                <i class="fas fa-dice-d20 text-primary"></i>
                                                            @else
                                                                <i class="fas fa-dice-d20"></i>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/download/index.js') }}" type="text/javascript"></script>
@endsection
