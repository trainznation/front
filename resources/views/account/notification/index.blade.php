{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

{{-- Content --}}
@section('content')
<div class="container">
    <div class="alert alert-custom alert-light-primary fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon-information"></i>
        </div>
        <div class="alert-text">Les notifications sont réinitialiser tous les dimanche à 00h00</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">
					<i class="ki ki-close"></i>
				</span>
            </button>
        </div>
    </div>
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">Mes Notifications ({{ count($notifications) }})</h3>
            </div>
            <div class="card-toolbar">
                <a href="{{ route('Notification.readAll') }}" id="btnReadAllNotif" class="btn btn-primary font-weight-bolder"><i class="la la-check"></i> Lire toutes les notifications</a>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-striped" id="listeNotifications">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Notification</th>
                        <th>Lu</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($notifications as $notification)
                    <tr>
                        @if($notification->status == 'success')
                            <td class="bg-success">&nbsp;</td>
                        @elseif($notification->status == 'warning')
                            <td class="bg-warning">&nbsp;</td>
                        @elseif($notification->status == 'danger')
                            <td class="bg-danger">&nbsp;</td>
                        @else
                            <td class="bg-info">&nbsp;</td>
                        @endif
                        <td>
                            <div class="d-flex align-items-center mb-10">
                                <!--begin::Symbol-->
                                <div class="symbol symbol-40 symbol-light-primary mr-5">
									<span class="symbol-label">
										<i class="{{ $notification->icon }}"></i>
									</span>
                                </div>
                                <!--end::Symbol-->
                                <!--begin::Text-->
                                <div class="d-flex flex-column font-weight-bold">
                                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">{!! $notification->title !!}</a>
                                    <span class="text-muted">{!! $notification->description !!}</span>
                                </div>
                                <!--end::Text-->
                            </div>
                        </td>
                        <td>
                            @if($notification->read_at !== null)
                                <i class="fas fa-check-circle text-success" data-toggle="tooltip" title="Lu le {{ \Illuminate\Support\Carbon::createFromTimestamp(strtotime($notification->read_at))->format('d/m/H à H:i') }}"></i>
                            @else
                                <a data-href="{{ route('Notification.read', $notification->id) }}" class="btn btn-link btn-icon btn-read"><i class="fas fa-times-circle text-danger"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}"></script>
    <script src="{{ asset('js/account/notification/index.js') }}" type="text/javascript"></script>
@endsection
