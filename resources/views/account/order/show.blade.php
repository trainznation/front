{{-- Extends layout --}}
@extends('layout.default')

@section('styles')

@endsection

@section('page_toolbar')
    <a href="{{ route('Order.index') }}" class="btn btn-sm btn-light"><i class="fas fa-arrow-circle-left"></i> Retour</a>
@endsection

{{-- Content --}}
@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-body p-0">
                <!--begin::Invoice-->
                <!--begin::Invoice header-->
                <div class="container">
                    <div class="card card-custom card-shadowless">
                        <div class="card-body p-0">
                            <div class="row justify-content-center py-8 px-8 py-md-27 px-md-0">
                                <div class="col-md-9">
                                    <div class="d-flex justify-content-between align-items-center flex-column flex-md-row">
                                        <div class="d-flex flex-column px-0 order-2 order-md-1 align-items-center align-items-md-start">
                                            <!--begin::Logo-->
                                            <a href="#" class="mb-5 max-w-115px">
												<img src="{{ asset('media/logos/logo_auth.png') }}" class="img-fluid" alt>
                                            </a>
                                            <!--end::Logo-->
                                            <span class="d-flex flex-column font-size-h5 font-weight-bold text-muted align-items-center align-items-md-start">
												<span class="font-weight-bold">Trainznation</span>
                                                <span>22 Rue Maryse Bastié</span>
                                                <span>85100 Les Sables d'Olonne</span>
                                                <span><i class="fas fa-envelope"></i>: trainznation@gmail.com</span>
											</span>
                                        </div>
                                        <h1 class="display-3 font-weight-boldest order-1 order-md-2 mb-5 mb-md-0">COMMANDE</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Invoice header-->
                <!--begin::Invoice Body-->
                <div class="position-relative">
                    <!--begin::Background Rows-->
                    <div class="bgi-size-cover bgi-position-center bgi-no-repeat h-65px" style="background-image: url({{ asset('media/svg/shapes/abstract-7.svg') }});"></div>
                    <div class="bg-white h-65px"></div>
                    <div class="bg-light h-65px"></div>
                    <div class="bg-white h-65px"></div>
                    <div class="bg-light h-65px"></div>
                    <!--end::Background Rows-->
                    <!--begin:Table-->
                    <div class="container position-absolute top-0 left-0 right-0">
                        <div class="row justify-content-center">
                            <div class="col-md-9">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr class="font-weight-boldest text-white h-65px">
                                            <td class="align-middle font-size-h4 pl-0 border-0">Désignation</td>
                                            <td class="align-middle font-size-h4 text-right border-0">Quantité</td>
                                            <td class="align-middle font-size-h4 text-right border-0">Prix Unitaire</td>
                                            <td class="align-middle font-size-h4 text-right pr-0 border-0">Total</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->products as $product)
                                        <tr class="font-size-lg font-weight-bolder h-65px">
                                            <td class="align-middle pl-0 border-0">
                                                <div class="symbol symbol-30 mr-3">
                                                    <img alt="Pic" src="{{ $product->asset->image }}"/>
                                                </div>
                                                {{ $product->name }}
                                            </td>
                                            <td class="align-middle text-right border-0">{{ $product->quantity }}</td>
                                            <td class="align-middle text-right border-0">{{ number_format($product->asset->price, 2, ',', ' ') }} €</td>
                                            <td class="align-middle text-right text-danger font-weight-boldest font-size-h5 pr-0 border-0">{{ number_format($product->total_price_gross, 2, ',', ' ') }} €</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end:Table-->
                    <!--begin::Total-->
                    <div class="container">
                        <div class="row justify-content-center pt-25 pb-20">
                            <div class="col-md-9">
                                <div class="rounded d-flex align-items-center justify-content-between text-white max-w-425px position-relative ml-auto px-7 py-5 bgi-no-repeat bgi-size-cover bgi-position-center" style="background-image: url({{ asset('media/svg/shapes/abstract-9.svg') }});">
                                    <div class="font-weight-boldest font-size-h5">Montant Total</div>
                                    <div class="text-right d-flex flex-column">
                                        <span class="font-weight-boldest font-size-h3 line-height-sm">{{ number_format($order->total, 2, ',', ' ') }} €</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end::Total-->
                </div>
                <!--end::Invoice Body-->
                <!--begin::Invoice Footer-->
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    @if($order->status == 1)
                                        <div class="d-flex flex-column flex-md-row">
                                            <div class="d-flex flex-column">
                                                <div class="font-weight-bold font-size-h6 mb-3">Payment</div>
                                                <div class="d-flex justify-content-between font-size-lg mb-3">
                                                    <span class="font-weight-bold mr-15">Type de Paiement:</span>
                                                    <span class="text-right"><i class="fas fa-credit-card"></i> Carte Bancaire</span>
                                                </div>
                                                <div class="d-flex justify-content-between font-size-lg mb-3">
                                                    <span class="font-weight-bold mr-15">Numéro de transaction:</span>
                                                    <span class="text-right">{{ $order->payment->payment_id }}</span>
                                                </div>
                                                <div class="d-flex justify-content-between font-size-lg">
                                                    <span class="font-weight-bold mr-15">Date de Transaction:</span>
                                                    <span class="text-right">{{ $order->payment->updated_at->normalize }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <a href="{{ route('Checkout.payment', ["order_id" => $order->id]) }}" class="btn btn-primary"><i class="fas fa-credit-card"></i> Payer cette commande</a>
                                    @endif
                                </div>
                                <div class="col-md-6 mt-7 mt-md-0">
                                    <!--begin::Invoice To-->
                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3 text-right">Adresser à.</div>
                                    <div class="font-size-lg font-weight-bold mb-10 text-right">
                                        {{ $order->user->name }}<br>
                                        <span class="text-muted font-size-sm">{{ $order->user->email }}</span>
                                    </div>
                                    <!--end::Invoice To-->
                                    <!--begin::Invoice No-->
                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3 text-right">COMMANDE N°.</div>
                                    <div class="font-size-lg font-weight-bold mb-10 text-right">{{ $order->reference }}</div>
                                    <!--end::Invoice No-->
                                    <!--begin::Invoice Date-->
                                    <div class="text-dark-50 font-size-lg font-weight-bold mb-3 text-right">DATE</div>
                                    <div class="font-size-lg font-weight-bold text-right">{{ $order->updated_at->normalize }}</div>
                                    <!--end::Invoice Date-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')

@endsection
