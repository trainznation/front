{{-- Extends layout --}}
@extends('layout.default')

@section('styles')

@endsection

{{-- Content --}}
@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Liste de mes commandes & factures</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Search Form-->
            <!--begin::Search Form-->
            <div class="mb-7">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Rechercher..." id="search_liste_order" />
                                    <span>
										<i class="flaticon2-search-1 text-muted"></i>
									</span>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                    <select class="form-control" id="search_liste_status">
                                        <option value="">Tous</option>
                                        <option value="0">Non Payé</option>
                                        <option value="1">Payé</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Search Form-->
            <!--end: Search Form-->
            <!--begin: Datatable-->
            <table class="datatable datatable-bordered datatable-head-custom" id="liste_order">
                <thead>
                <tr>
                    <th title="Field #1">Id</th>
                    <th title="Field #1">Date de la commande</th>
                    <th title="Field #1">Total</th>
                    <th title="Field #1">Status</th>
                    <th title="Field #1">Actions</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->updated_at->normalize }}</td>
                            <td>{{ number_format($order->total, 2, ',', ' ') }} €</td>
                            <td>{{ $order->status }}</td>
                            <td></td>
                        </tr>
                    @endforeach
                    @foreach($invoices as $invoice)
                        <tr>
                            <td>{{ $invoice->id }}</td>
                            <td>{{ \Illuminate\Support\Carbon::createFromTimestamp($invoice->created)->format('d/m/Y') }}</td>
                            <td>{{ \App\Helpers\Calculator::numberFormat($invoice->amount_due / 100) }}</td>
                            <td>
                                @if($invoice->paid == false) 0 @else 1 @endif
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/account/order/index.js') }}" type="text/javascript"></script>
@endsection
