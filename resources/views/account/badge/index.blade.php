{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-body">
                @foreach($badges as $badge)
                <div class="d-flex align-items-center mb-10">
                    <!--begin::Symbol-->
                    <div class="symbol symbol-40 symbol-light-primary mr-5">
						<div class="symbol-label" style="background-image: url('{{ env('DOWN_ENDPOINT') }}/achievement/{{ $badge->details->name }}.png')"></div>
                    </div>
                    <!--end::Symbol-->
                    <!--begin::Text-->
                    <div class="d-flex flex-column font-weight-bold">
                        <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">{{ $badge->details->description }}</a>
                        <span class="text-muted">
                            @if($badge->unlocked_at->format >= now()->format('00:00:00') && $badge->unlocked_at->format <= now()->format('23:59:59'))
                                {{ $badge->unlocked_at->human }}
                            @else
                                {{ $badge->unlocked_at->normalize }}
                            @endif
                        </span>
                    </div>
                    <!--end::Text-->
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/account/profil.js') }}" type="text/javascript"></script>
@endsection
