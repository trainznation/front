{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="d-flex">
                <!--begin::Pic-->
                <div class="flex-shrink-0 mr-7">
                    <div class="symbol symbol-50 symbol-lg-120">
                        <img alt="Pic" src="{{ $user->avatar->encoded }}">
                    </div>
                </div>
                <!--end::Pic-->
                <!--begin: Info-->
                <div class="flex-grow-1">
                    <!--begin::Title-->
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <!--begin::User-->
                        <div class="mr-3">
                            <div class="d-flex align-items-center mr-3">
                                <!--begin::Name-->
                                <a href="#" class="d-flex align-items-center text-dark text-hover-primary font-size-h5 font-weight-bold mr-3">{{ $user->name }}</a>
                                <!--end::Name-->
                                @if($user->admin == 1)
                                <span class="label label-light-danger label-inline font-weight-bolder mr-1">Administrateur</span>
                                @else
                                    <span class="label label-light-success label-inline font-weight-bolder mr-1">Utilisateur</span>
                                @endif
                            </div>
                            <!--begin::Contacts-->
                            <div class="d-flex flex-wrap my-2">
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
								    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
								    	<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Mail-notification.svg-->
								    	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								    		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								    			<rect x="0" y="0" width="24" height="24"></rect>
								    			<path d="M21,12.0829584 C20.6747915,12.0283988 20.3407122,12 20,12 C16.6862915,12 14,14.6862915 14,18 C14,18.3407122 14.0283988,18.6747915 14.0829584,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,8 C3,6.8954305 3.8954305,6 5,6 L19,6 C20.1045695,6 21,6.8954305 21,8 L21,12.0829584 Z M18.1444251,7.83964668 L12,11.1481833 L5.85557487,7.83964668 C5.4908718,7.6432681 5.03602525,7.77972206 4.83964668,8.14442513 C4.6432681,8.5091282 4.77972206,8.96397475 5.14442513,9.16035332 L11.6444251,12.6603533 C11.8664074,12.7798822 12.1335926,12.7798822 12.3555749,12.6603533 L18.8555749,9.16035332 C19.2202779,8.96397475 19.3567319,8.5091282 19.1603533,8.14442513 C18.9639747,7.77972206 18.5091282,7.6432681 18.1444251,7.83964668 Z" fill="#000000"></path>
								    			<circle fill="#000000" opacity="0.3" cx="19.5" cy="17.5" r="2.5"></circle>
								    		</g>
								    	</svg>
                                        <!--end::Svg Icon-->
								    </span>
                                    {{ $user->email }}
                                </a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
								    <span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
								    	<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Lock.svg-->
								    	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
								    		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								    			<mask fill="white">
								    				<use xlink:href="#path-1"></use>
								    			</mask>
								    			<g></g>
								    			<path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"></path>
								    		</g>
								    	</svg>
                                        <!--end::Svg Icon-->
								    </span>
                                    @if($user->account->premium == 1)
                                        Compte Premium
                                    @else
                                        Compte Normal
                                    @endif
                                </a>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold">
															<span class="svg-icon svg-icon-md svg-icon-gray-500 mr-1">
																<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Map/Marker2.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"></rect>
																		<path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>{{ $location->countryName }}, {{ $location->regionName }}, {{ $location->cityName }} ({{ $user->account->last_ip }})</a>
                            </div>
                            <!--end::Contacts-->
                        </div>
                        <!--begin::User-->
                        <!--begin::Actions-->
                        <div class="mb-10">
                            @if($user->account->premium == 0)
                            <a href="{{ route('Account.Premium.index') }}" class="btn btn-sm btn-light-warning font-weight-bolder text-uppercase mr-2">Souscrire à l'offre premium</a>
                            @endif
                        </div>
                        <!--end::Actions-->
                    </div>
                    <!--end::Title-->
                    <!--begin::Content-->
                    <!--end::Content-->
                </div>
                <!--end::Info-->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <h3 class="card-title">Vos Informations</h3>
                </div>
                <div class="card-body">
                    <div class="form-group row my-2">
                        <label class="col-md-4 col-form-label">Nom</label>
                        <div class="col-md-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $user->name }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-md-4 col-form-label">Email</label>
                        <div class="col-md-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $user->email }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-md-4 col-form-label">Compte créer le</label>
                        <div class="col-md-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ \Carbon\Carbon::createFromTimestamp(strtotime($user->created_at))->format('d/m/Y à H:i') }} ({{ \Carbon\Carbon::createFromTimestamp(strtotime($user->created_at))->diffForHumans() }})</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-md-4 col-form-label">Site Web</label>
                        <div class="col-md-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $user->account->site_web }}</span>
                        </div>
                    </div>
                    <div class="form-group row my-2">
                        <label class="col-md-4 col-form-label">Point</label>
                        <div class="col-md-8">
                            <span class="form-control-plaintext font-weight-bolder">{{ $user->account->point }} Trainzpoint <i class="fas fa-info-circle text-primary" data-toggle="tooltip" title="Les Trainzpoint peuvent permettre d'acheter les objets payants avec une reduction"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card card-custom">
                <div class="card-header card-header-tabs-line">
                    <div class="card-toolbar">
                        <ul class="nav nav-tabs nav-bold nav-tabs-line">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#editing">
                                    <span class="nav-icon"><i class="flaticon2-edit"></i></span>
                                    <span class="nav-text">Editer mes informations</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#social">
                                    <span class="nav-icon"><i class="socicon socicon-facebook"></i></span>
                                    <span class="nav-text">Social</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#password">
                                    <span class="nav-icon"><i class="flaticon2-lock"></i></span>
                                    <span class="nav-text">Changer mon mot de passe</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#cards">
                                    <span class="nav-icon"><i class="fas fa-credit-card"></i></span>
                                    <span class="nav-text">Moyen de paiement</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#invoices">
                                    <span class="nav-icon"><i class="fas fa-euro-sign"></i></span>
                                    <span class="nav-text">Facturations</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#premium">
                                    <span class="nav-icon"><i class="fas fa-certificate"></i></span>
                                    <span class="nav-text">Premium</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="editing" role="tabpanel">
                            <div class="font-weight-bold display-4">Editer mes informations</div>
                            <form id="formEditing" action="{{ route('Account.editing') }}" method="post" class="mt-5 mb-5">
                                <div class="form-group">
                                    <label for="name">Identité (nom ou pseudo)</label>
                                    <input type="text" class="form-control" name="name" autofocus="false" value="{{ $user->name }}">
                                </div>

                                <div class="form-group">
                                    <label for="email">Adresse email</label>
                                    <input type="email" class="form-control" name="email" autofocus="false" value="{{ $user->email }}">
                                </div>

                                <div class="form-group">
                                    <label for="site_web">Site Web</label>
                                    <input type="url" class="form-control" name="site_web" autofocus="false" value="{{ $user->account->site_web }}">
                                </div>

                                <button type="submit" id="btnSubmitEditing" class="btn btn-outline-primary">Valider</button>
                            </form>
                            <div class="separator separator-solid separator-border-2 mb-5"></div>
                            <div class="card card-custom bgi-no-repeat bgi-size-cover gutter-b bg-danger" style="height: 250px; background-image: url({{ asset('media/svg/icons/code/Warning-2.svg') }})">
                                <div class="card-body d-flex">
                                    <div class="d-flex py-5 flex-column align-items-start flex-grow-1">
                                        <div class="flex-grow-1">
                                            <a href="#" class="text-white font-weight-bolder font-size-h3">ZONE DE DANGER</a>
                                            <p class="text-white opacity-75 font-weight-bold mt-3">Voulez-vous supprimer votre compte</p>
                                        </div>
                                        <a href="#deleteAccount" data-toggle="modal" class="btn btn-link btn-link-white font-weight-bold">Supprimer
                                            <span class="svg-icon svg-icon-lg svg-icon-white">
												<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Arrow-right.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-90.000000) translate(-12.000000, -12.000000)" x="11" y="5" width="2" height="14" rx="1"></rect>
														<path d="M9.70710318,15.7071045 C9.31657888,16.0976288 8.68341391,16.0976288 8.29288961,15.7071045 C7.90236532,15.3165802 7.90236532,14.6834152 8.29288961,14.2928909 L14.2928896,8.29289093 C14.6714686,7.914312 15.281055,7.90106637 15.675721,8.26284357 L21.675721,13.7628436 C22.08284,14.136036 22.1103429,14.7686034 21.7371505,15.1757223 C21.3639581,15.5828413 20.7313908,15.6103443 20.3242718,15.2371519 L15.0300721,10.3841355 L9.70710318,15.7071045 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.999999, 11.999997) scale(1, -1) rotate(90.000000) translate(-14.999999, -11.999997)"></path>
													</g>
												</svg>
                                            <!--end::Svg Icon-->
											</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="social" role="tabpanel">
                            <div class="font-weight-bold display-4 mb-4">Connexion au réseau sociaux</div>
                            <div class="alert alert-primary" role="alert">Bientôt disponible </div>
                        </div>
                        <div class="tab-pane fade" id="password" role="tabpanel">
                            <div class="font-weight-bold display-4 mb-5">Changement de mot de passe</div>
                            <form id="formChangePassword" action="{{ route('Account.changePassword') }}" method="post">
                                @method('PUT')
                                <div class="form-group">
                                    <label>Nouveau Mot de passe</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <button type="submit" id="btnSubmitChangePassword" class="btn btn-primary">Valider</button>
                            </form>

                        </div>
                        <div class="tab-pane fade" id="cards" role="tabpanel">
                            <div class="card card-custom">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">Moyen de paiement</h3>
                                    </div>
                                    <div class="card-toolbar">
                                        <button class="btn btn-outline-primary btn-shadow"><i class="fas fa-plus-circle"></i> Nouveau moyen de paiement</button>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Type de Paiement</th>
                                                <th>Compte</th>
                                                <th>Expiration</th>
                                                <th>Etat</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cards as $card)
                                                <tr>
                                                    <td><i class="fas fa-credit-card"></i> Carte Bancaire</td>
                                                    <td>XXXX XXXX XXXX {{ $card->last4 }}</td>
                                                    <td>{{ ($card->exp_month >= 1 && $card->exp_month <= 9) ? '0'.$card->exp_month : $card->exp_month }}/{{ $card->exp_year }}</td>
                                                    <td class="text-center">
                                                        @if(\Illuminate\Support\Carbon::create($card->exp_year, $card->exp_month, 30) <= now())
                                                            <span class="label label-inline label-danger">Expiré</span>
                                                        @elseif(\Illuminate\Support\Carbon::create($card->exp_year, $card->exp_month, 30) >= now())
                                                            <span class="label label-inline label-success">Valide</span>
                                                        @else
                                                            <span class="label label-inline label-warning"><i class="fas fa-warning" data-toggle="tooltip" title="Veuillez enregistrer un nouveau moyen de paiement"></i> Expire Bientôt</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="" class="btn btn-xs btn-danger btn-icon" data-toggle="tooltip" title="Supprimer le mode de paiement"><i class="fas fa-trash"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="invoices" role="tabpanel">
                            <div class="card card-custom mb-5">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">Vos Commandes</h3>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Référence</th>
                                                <th>Date de la commande</th>
                                                <th>Montant</th>
                                                <th>Etat</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($orders) == 0)
                                                <tr>
                                                    <td colspan="5" class="text-center">Vous n'avez aucune commande</td>
                                                </tr>
                                            @else
                                                @foreach($orders as $order)
                                                    <tr>
                                                        <td>{{ $order->reference }}</td>
                                                        <td>{{ $order->updated_at->normalize }}</td>
                                                        <td>{{ number_format($order->total, 2, ',', ' ') }} €</td>
                                                        <td>
                                                            @if($order->status == 0)
                                                                <span class="label label-inline label-danger">Non Payé</span>
                                                            @else
                                                                <span class="label label-inline label-success">Payé</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('Order.show', $order->id) }}" class="btn btn-xs btn-primary btn-icon"><i class="fas fa-eye" data-toggle="tooltip" title="Voir la commande"></i> </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card card-custom mb-5">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3 class="card-label">Vos factures</h3>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>Référence</th>
                                            <th>Date de la commande</th>
                                            <th>Montant</th>
                                            <th>Etat</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($invoices) == 0)
                                            <tr>
                                                <td colspan="5" class="text-center">Vous n'avez aucune factures</td>
                                            </tr>
                                        @else
                                            @foreach($invoices as $invoice)
                                                <tr>
                                                    <td>{{ $invoice->number }}</td>
                                                    <td>{{ \Illuminate\Support\Carbon::createFromTimestamp($invoice->created)->format('d/m/Y') }}</td>
                                                    <td>{{ number_format($invoice->amount_due/100, 2, ',', ' ') }} €</td>
                                                    <td>
                                                        @if($invoice->paid == false)
                                                            <span class="label label-inline label-danger">Non Payé</span>
                                                        @else
                                                            <span class="label label-inline label-success">Payé</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ $invoice->hosted_invoice_url }}" class="btn btn-xs btn-primary btn-icon"><i class="fas fa-eye" data-toggle="tooltip" title="Voir la facture"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="premium" role="tabpanel">
                            @if($user->account->premium == 0)
                                <div class="alert alert-primary text-center" role="alert">
                                    Votre compte n'est pas un compte de type <strong>Premium</strong>.
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-custom bg-success gutter-b" style="height: 200px">
                                            <div class="card-body">
												<span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
													<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24"></rect>
															<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"></rect>
															<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3"></path>
														</g>
													</svg>
                                                    <!--end::Svg Icon-->
												</span>
                                                <div class="text-inverse-success font-weight-bolder font-size-h2 mt-3">Date de souscription</div>
                                                <a class="text-inverse-success font-weight-bold font-size-lg mt-1">{{ \Carbon\Carbon::createFromTimestamp($subscription->created)->format('d/m/Y') }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-custom bg-info gutter-b" style="height: 200px">
                                            <div class="card-body">
												<span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
													<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24"></rect>
															<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"></rect>
															<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3"></path>
														</g>
													</svg>
                                                    <!--end::Svg Icon-->
												</span>
                                                <div class="text-inverse-success font-weight-bolder font-size-h2 mt-3">Plan</div>
                                                <a class="text-inverse-success font-weight-bold font-size-lg mt-1">{{ $subscription->plan->nickname }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-custom bg-danger gutter-b" style="height: 200px">
                                            <div class="card-body">
												<span class="svg-icon svg-icon-3x svg-icon-white ml-n2">
													<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24"></rect>
															<rect fill="#000000" x="4" y="4" width="7" height="7" rx="1.5"></rect>
															<path d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z" fill="#000000" opacity="0.3"></path>
														</g>
													</svg>
                                                    <!--end::Svg Icon-->
												</span>
                                                <div class="text-inverse-success font-weight-bolder font-size-h2 mt-3">Prochaine facture</div>
                                                <a class="text-inverse-success font-weight-bold font-size-lg mt-1">{{ \Illuminate\Support\Carbon::createFromTimestamp($subscription->current_period_end)->format('d/m/Y') }}<br> <span class="text-muted">{{ number_format($subscription->plan->amount / 100, 2, ',', ' ') }} €</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="{{ route('Account.Premium.desactivate', ["sub" => $subscription->id]) }}" class="btn btn-danger btn-block btn-lg">Annuler mon abonnement</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h5 class="modal-title  text-white" id="exampleModalLabel">Suppression de compte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close  text-white"></i>
                </button>
            </div>
            <div class="modal-body text-center">
                <span class="svg-icon svg-icon-3x svg-icon-danger">
					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Arrow-right.svg-->
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70px" height="70px" viewBox="0 0 24 24" version="1.1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M11.1669899,4.49941818 L2.82535718,19.5143571 C2.557144,19.9971408 2.7310878,20.6059441 3.21387153,20.8741573 C3.36242953,20.9566895 3.52957021,21 3.69951446,21 L21.2169432,21 C21.7692279,21 22.2169432,20.5522847 22.2169432,20 C22.2169432,19.8159952 22.1661743,19.6355579 22.070225,19.47855 L12.894429,4.4636111 C12.6064401,3.99235656 11.9909517,3.84379039 11.5196972,4.13177928 C11.3723594,4.22181902 11.2508468,4.34847583 11.1669899,4.49941818 Z" fill="#000000" opacity="0.3"/>
                            <rect fill="#000000" x="11" y="9" width="2" height="7" rx="1"/>
                            <rect fill="#000000" x="11" y="17" width="2" height="2" rx="1"/>
                        </g>
					</svg>
				</span>
                <p class="font-weight-bold border border-danger">La suppression du compte est définitive !</p>
            </div>
            <div class="modal-footer">
                <a type="button" href="{{ route('Account.delete') }}" class="btn btn-danger font-weight-bold">Supprimer mon compte</a>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/account/profil.js') }}" type="text/javascript"></script>
@endsection
