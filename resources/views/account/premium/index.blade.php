{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom">
        <div class="card-body">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr class="bg-primary">
                        <th class="bg-white"></th>
                        <th class="text-center text-white">
                            <div class="font-weight-bold font-size-h1">Mensuel</div>
                            <span class="font-size-h3 d-block font-weight-boldest py-2">3,50 <sup class="font-size-h3 font-weight-normal pl-1">€</sup></span>
                        </th>
                        <th class="text-center text-white">
                            <div class="font-weight-bold font-size-h1">Trimestriel</div>
                            <span class="font-size-h3 d-block font-weight-boldest py-2">10,00 <sup class="font-size-h3 font-weight-normal pl-1">€</sup></span>
                        </th>
                        <th class="text-center text-white">
                            <div class="font-weight-bold font-size-h1">Semestriel</div>
                            <span class="font-size-h3 d-block font-weight-boldest py-2">19,00 <sup class="font-size-h3 font-weight-normal pl-1">€</sup></span>
                        </th>
                        <th class="text-center text-white">
                            <div class="font-weight-bold font-size-h1">Annuel</div>
                            <span class="font-size-h3 d-block font-weight-boldest py-2">35,00 <sup class="font-size-h3 font-weight-normal pl-1">€</sup></span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="p-lg-5">
                        <td class="align-middle">
                            <span class="font-weight-bold">Accès aux vidéos premium</span>
                        </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                    </tr>
                    <tr class="p-lg-5">
                        <td class="align-middle">
                            <span class="font-weight-bold">Accès aux resources des vidéos</span>
                        </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                    </tr>
                    <tr class="p-lg-5">
                        <td class="align-middle">
                            <span class="font-weight-bold">Réduction sur la boutique d'objet</span>
                        </td>
                        <td class="text-center"> 5% </td>
                        <td class="text-center"> 10% </td>
                        <td class="text-center"> 25% </td>
                        <td class="text-center"> 35% </td>
                    </tr>
                    <tr class="p-lg-5">
                        <td class="align-middle">
                            <span class="font-weight-bold">Asset Beta <i class="fas fa-info-circle text-primary" data-toggle="popover" title="Information" data-content="Accès au béta des différentes créations ainsi que la libération des FBX et Textures"></i> </span>
                        </td>
                        <td class="text-center"><i class="far fa-times-circle fa-2x text-danger"></i> </td>
                        <td class="text-center"><i class="far fa-times-circle fa-2x text-danger"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                        <td class="text-center"><i class="far fa-check-circle fa-2x text-success"></i> </td>
                    </tr>
                </tbody>
            </table>
            <a href="{{ route('Account.Premium.subscribe') }}" class="btn btn-lg btn-block btn-primary"><i class="fas fa-credit-card"></i> Souscrire à l'offre premium</a>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
@endsection
