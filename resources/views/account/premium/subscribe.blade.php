{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-header">
                <h3 class="card-title">{{ $page_title }}</h3>
            </div>
            <div class="card-body">
                <form action="{{ route('Account.Premium.activate') }}" id="formSubscribe" method="post">
                    <input type="hidden" id="stripeToken" name="stripeToken">
                    <input type="hidden" id="customerId" name="customer_id" value="{{ session()->get('user')->account->customer_id }}">
                    <div class="form-group">
                        <label>Plan Tarifaire</label>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="option">
								<span class="option-control">
									<span class="radio">
										<input type="radio" name="plan" value="mensuel">
										<span></span>
									</span>
								</span>
                                    <span class="option-label">
									<span class="option-head">
										<span class="option-title">Mensuel</span>
										<span class="option-focus">3,50 €</span>
									</span>
								</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="option">
								<span class="option-control">
									<span class="radio">
										<input type="radio" name="plan" value="trimestriel">
										<span></span>
									</span>
								</span>
                                    <span class="option-label">
									<span class="option-head">
										<span class="option-title">Trimestriel</span>
										<span class="option-focus">10,00 €</span>
									</span>
								</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="option">
								<span class="option-control">
									<span class="radio">
										<input type="radio" name="plan" value="semestriel">
										<span></span>
									</span>
								</span>
                                    <span class="option-label">
									<span class="option-head">
										<span class="option-title">Semestriel</span>
										<span class="option-focus">19,00 €</span>
									</span>
								</span>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label class="option">
								<span class="option-control">
									<span class="radio">
										<input type="radio" name="plan" value="annuel">
										<span></span>
									</span>
								</span>
                                    <span class="option-label">
									<span class="option-head">
										<span class="option-title">Annuel</span>
										<span class="option-focus">35,00 €</span>
									</span>
								</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Identité</label>
                        <input type="text" id="name" class="form-control" name="name" placeholder="Durand Jean"
                               required>
                    </div>
                    <div class="form-group">
                        <label>Adresse Mail</label>
                        <input type="email" class="form-control" name="email"
                               value="{{ session()->get('user')->email }}" required>
                    </div>
                    <div class="form-group">
                        <label>Adresse Postal</label>
                        <input type="text" class="form-control" name="address" placeholder="12 rue des hauvent"
                               required>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Code Postal</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="code_postal" placeholder="44600" required>
                        </div>
                        <label class="col-form-label col-md-2">Ville</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="ville" placeholder="Saint Nazaire" required>
                        </div>
                        <label class="col-form-label col-md-2">Pays</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="pays" value="France" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Carte Bancaire</label>
                        @if(count(\App\Helpers\StripeClass::getListCards()) != 0)
                            <div id="cards">
                                <div class="row">
                                    @foreach(\App\Helpers\StripeClass::getListCards() as $card)
                                        <div class="col-md-4">
                                            <label class="option">
											<span class="option-control">
												<span class="radio">
													<input type="radio" name="stripeCard" value="{{ $card->id }}">
													<span></span>
												</span>
											</span>
                                                <span class="option-label">
												<span class="option-head">
													<span class="option-title">XXXX XXXX XXXX {{ $card->last4 }}</span>
												</span>
												<span class="option-body">Expire: {{ ($card->exp_month >= 1 && $card->exp_month <= 9) ? '0'.$card->exp_month : $card->exp_month }}/{{ $card->exp_year }}</span>
											</span>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @else
                            <div id="newCard">
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <div id="card-element"></div>
                                        <p id="card-error" role="alert"></p>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <button type="submit" id="btnFormSubscribe" class="btn btn-primary btn-block">Souscrire</button>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">

        let constElement = {
            divCards: KTUtil.getById('cards'),
            divNewCards: KTUtil.getById('newCard'),
            displayError: document.getElementById('card-errors'),
            formSubscribe: KTUtil.getById('formSubscribe'),
            stripeToken: KTUtil.getById('stripeToken')
        }

        if(constElement.divNewCards) {
            let stripe = Stripe('{{ config('stripe.publishable_key') }}')
            let elements = stripe.elements();

            let style = {
                base: {
                    color: "#32325d",
                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                    fontSmoothing: "antialiased",
                    fontSize: "16px",
                    "::placeholder": {
                        color: "#aab7c4"
                    }
                },
                invalid: {
                    color: "#fa755a",
                    iconColor: "#fa755a"
                }
            };

            let cardElement = elements.create("card", {style: style});
            cardElement.mount("#card-element");

            cardElement.on('change', function (event) {
                if (event.error) {
                    constElement.displayError.textContent = event.error.message;
                } else {
                    constElement.displayError.textContent = '';
                }
            });

            constElement.formSubscribe.addEventListener('submit', (e) => {
                e.preventDefault()

                stripe.createToken(cardElement).then((result) => {
                    if(result.error) {
                        constElement.displayError.textContent = result.error.message
                    } else {
                        stripeTokenHandler(result.token)
                    }
                })
            })

            function stripeTokenHandler(token) {
                constElement.stripeToken.value = token.id
                constElement.formSubscribe.submit()
            }
        } else {
            constElement.formSubscribe.addEventListener('submit', (e) => {
                e.preventDefault()

                constElement.formSubscribe.submit()
            })
        }

    </script>
@endsection
