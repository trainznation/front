{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
<div class="container">
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Objets téléchargeables</h3>
        </div>
        <div class="card-body">
            @foreach($downloads as $download)
                <div class="d-flex align-items-center mb-10">
                    <!--begin::Symbol-->
                    <div class="symbol symbol-75 symbol-2by3 mr-5">
					    <span class="symbol-label" style="background-image: url('{{ $download->asset->image }}')"></span>
                    </div>
                    <!--end::Symbol-->
                    <!--begin::Text-->
                    <div class="d-flex flex-column flex-grow-1 font-weight-bold">
                        <a href="{{ route('Download.show', $download->asset->id) }}" class="text-dark text-hover-primary mb-1 font-size-lg">{{ $download->asset->designation }}</a>
                        <span class="text-muted">Débloquer le {{ $download->unlocked_at->normalize }}</span>
                    </div>
                    <!--end::Text-->
                    <!--begin::Dropdown-->
                    @if($download->asset->link_uuid !== null)
                        <a href="{{ $download->asset->link_uuid }}" class="btn btn-primary btn-icon btn-shadow" data-toggle="tooltip" title="Télécharger"><i class="fas fa-download"></i></a>
                    @else
                        <i class="far fa-clock text-warning fa-3x" data-toggle="tooltip" title="Téléchargement indisponible actuellement"></i>
                    @endif
                    <!--end::Dropdown-->
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
@endsection
