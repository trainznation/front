{{-- Extends layout --}}
@extends('layout.default')

@section('styles')
    <link href="https://vjs.zencdn.net/7.8.4/video-js.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('plugins/custom/lightbox/css/lightbox.css') }}">
@endsection

{{-- Content --}}
@section('content')
    <div class="container-fluid">
        <div class="card card-custom gutter-b">
            <!--begin::Body-->
            <div class="card-body">
                <!--begin::Nav Tabs-->
                <ul class="dashboard-tabs nav nav-pills nav-primary row row-paddingless m-0 p-0 flex-column flex-sm-row" role="tablist">
                    <!--begin::Item-->
                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center active" data-toggle="pill" href="#description">
							<span class="nav-icon py-2 w-auto">
								<i class="far fa-building fa-3x"></i>
							</span>
                            <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Description</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#version">
							<span class="nav-icon py-2 w-auto">
								<i class="fas fa-code-branch fa-3x"></i>
							</span>
                            <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Version</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#gallery">
							<span class="nav-icon py-2 w-auto">
								<i class="far fa-images fa-3x"></i>
							</span>
                            <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Gallery</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#roadmap">
							<span class="nav-icon py-2 w-auto">
								<i class="fas fa-wrench fa-3x"></i>
							</span>
                            <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Roadmap</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#download">
							<span class="nav-icon py-2 w-auto">
								<i class="fas fa-download fa-3x"></i>
							</span>
                            <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Téléchargement</span>
                        </a>
                    </li>
                    <li class="nav-item d-flex col flex-grow-1 flex-shrink-0 mr-3 mb-3 mb-lg-0">
                        <a class="nav-link border py-10 d-flex flex-grow-1 rounded flex-column align-items-center" data-toggle="pill" href="#session">
							<span class="nav-icon py-2 w-auto">
								<i class="fas fa-th-list fa-3x"></i>
							</span>
                            <span class="nav-text font-size-lg py-2 font-weight-bold text-center">Sessions</span>
                        </a>
                    </li>
                    <!--end::Item-->
                </ul>
                <!--end::Nav Tabs-->
            </div>
            <!--end::Body-->
        </div>
    </div>
    <div class="tab-content m-0 p-0">
        <div class="tab-pane active" id="description" role="tabpanel">
            <div class="container">
                <div class="card card-custom card-rounded no-gutters">
                    <div class="card-header bgi-position-center bgi-no-repeat bgi-size-cover" style="background: url('{{ $route->image }}') no-repeat; height: 450px;"></div>
                    <div class="card-body">
                        <div class="card card-custom card-rounded shadow-lg" style="height: 180px; margin-bottom: 55px;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        @if($latest_download->alpha == 1)
                                            <div class="symbol symbol-100 symbol-circle symbol-danger" style="position: relative; top: 10%; left: 15%" data-toggle="tooltip" title="Release Actuel">
                                                <span class="symbol-label font-size-h1">A</span>
                                            </div>
                                        @endif
                                        @if($latest_download->beta == 1)
                                            <div class="symbol symbol-100 symbol-circle symbol-warning" style="position: relative; top: 10%; left: 15%" data-toggle="tooltip" title="Release Actuel">
                                                <span class="symbol-label font-size-h1">B</span>
                                            </div>
                                        @endif
                                        @if($latest_download->release == 1)
                                            <div class="symbol symbol-100 symbol-circle symbol-success" style="position: relative; top: 10%; left: 15%" data-toggle="tooltip" title="Release Actuel">
                                                <span class="symbol-label font-size-h1">R</span>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <h1 class="font-weight-bold">Version actuel:</h1>
                                            <h3>{{ $latest_download->version }}</h3>
                                            <h1 class="font-weight-bold">Build actuel:</h1>
                                            <h3>{{ $latest_download->build }}</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div id="apex_general_tasks_average" style="height: 150px;" data-percent="{{ $latest_version->percent_task }}" data-toggle="tooltip" title="Avancement de la dernière version"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="font-size-lg" id="contentDescription" data-content="{{$route->description}}">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="version" role="tabpanel">
            <div class="container-fluid">
                <div class="card card-custom">
                    <div class="card-body">
                        <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample3">
                            @foreach($versions as $version)
                            <div class="card">
                                <div class="card-header" id="headingOne{{ $version->id }}">
                                    <div class="card-title" data-toggle="collapse" data-target="#collapseOne{{ $version->id }}">
                                        @if($version->published_at->format != null)
                                            <span class="label label-pill label-inline label-success mr-3">Publier</span>
                                        @else
                                            <span class="label label-pill label-inline label-danger mr-3">Non Publier</span>
                                        @endif
                                        Version {{ $version->version }}:{{ $version->build }}
                                    </div>
                                </div>
                                <div id="collapseOne{{ $version->id }}" class="collapse" data-parent="#accordionExample3">
                                    <div class="card-body">
                                        <div class="row mt-5">
                                            <div class="col-md-2">
                                                <div class="d-flex align-items-center mb-10 bg-gray">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                                        <div class="symbol-label">
                                                            <img src="{{ asset('media/pictogrammes/km.png') }}" class="h-75 align-self-end" alt="">
                                                        </div>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column font-weight-bold">
                                                        <span class="text-dark font-weight-bold mb-1 font-size-lg">Distance</span>
                                                        <span class="text-muted">{{ number_format($version->distance, 2, ',', ' ') }} Km</span>
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                                <div class="d-flex align-items-center mb-10 bg-gray">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                                        <div class="symbol-label">
                                                            <img src="{{ asset('media/pictogrammes/start.png') }}" class="h-75 align-self-end" alt="">
                                                        </div>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column font-weight-bold">
                                                        <span class="text-dark font-weight-bold mb-1 font-size-lg">Point de départ</span>
                                                        <span class="text-muted">{{ $version->station_start }}</span>
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                                <div class="d-flex align-items-center mb-10 bg-gray">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-40 symbol-light-white mr-5">
                                                        <div class="symbol-label">
                                                            <img src="{{ asset('media/pictogrammes/stop.png') }}" class="h-75 align-self-end" alt="">
                                                        </div>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column font-weight-bold">
                                                        <span class="text-dark font-weight-bold mb-1 font-size-lg">Point d'arriver</span>
                                                        <span class="text-muted">{{ $version->station_end }}</span>
                                                    </div>
                                                    <!--end::Text-->
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-9">
                                                <img src="{{ $version->image }}" class="img-fluid" width="1280" alt="">
                                                <div class="mt-5">
                                                    <video class="video-js vjs-default-skin videos" controls preload="none" poster="{{ $version->image }}" data-setup='{"width": "1000px;"}'>
                                                        <source src="{{ $version->video }}" type="video/mp4" />
                                                    </video>
                                                </div>
                                                <div class="mt-5">
                                                    <iframe width="100%" height="500px" frameborder="0" allowfullscreen src="{{ $version->map_link }}"></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="gallery" role="tabpanel">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card card-custom">
                            <div class="card-body">
                                <ul class="navi navi-accent navi-hover navi-bold js-filter">
                                    @foreach($route->gallery->categories as $category)
                                        <li class="navi-item">
                                            <a class="navi-link navi-gallery" data-href="#" data-categoryid="{{ $category->id }}" data-route="{{ $category->route_id }}">
                                                <span class="navi-text font-size-lg">{{ $category->name }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card card-custom gutter-b">
                            <div class="card-body">
                                <div class="row" id="js-content"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="roadmap" role="tabpanel">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card card-custom">
                            <div class="card-header h-auto border-0">
                                <div class="card-title py-4">
                                    <h3 class="card-label">
                                        <span class="d-block text-dark font-weight-bolder">Average Système Tasks</span>
                                        <span id="averageVersion" data-percent="0" class="d-block text-muted mt-2 font-size-sm">Version: {{ $latest_version->version }} | Build: {{ $latest_version->build }}</span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body d-flex flex-column">
                                <div class="flex-grow-1">
                                    <div id="apex_general_tasks_average_one" style="height: 250px;" data-percent="{{ $latest_version->percent_task }}" data-toggle="tooltip" title="Avancement de la dernière version"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card card-custom" style="height: 400px;">
                            <div class="card-body">
                                <div class="d-flex align-items-center mb-3 mt-15 bg-light rounded p-5">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-primary mr-5">
                                        <span class="svg-icon svg-icon-lg">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Library.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <circle fill="#000000" cx="12" cy="12" r="8"></circle>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Title-->
                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-h2 mb-1">Version</a>
                                    </div>
                                    <!--end::Title-->
                                    <!--begin::Lable-->
                                    <span id="textTaskRegistered" class="font-weight-bolder text-primary py-1 font-size-h2">{{ $latest_version->version }}</span>
                                    <!--end::Lable-->
                                </div>
                                <div class="d-flex align-items-center mb-3 mt-15 bg-light rounded p-5">
                                    <!--begin::Icon-->
                                    <span class="svg-icon svg-icon-primary mr-5">
                                        <span class="svg-icon svg-icon-lg">
                                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Home/Library.svg-->
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <circle fill="#000000" cx="12" cy="12" r="8"></circle>
                                                </g>
                                            </svg>
                                            <!--end::Svg Icon-->
                                        </span>
                                    </span>
                                    <!--end::Icon-->
                                    <!--begin::Title-->
                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                        <a href="#" class="font-weight-bold text-dark-75 text-hover-primary font-size-h2 mb-1">Build</a>
                                    </div>
                                    <!--end::Title-->
                                    <!--begin::Lable-->
                                    <span id="textTaskRegistered" class="font-weight-bolder text-primary py-1 font-size-h2">{{ $latest_version->build }}</span>
                                    <!--end::Lable-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-lg-5">
                    <div class="col-md-12">
                        <div class="card card-custom">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">{{ $route->name }} <small>{{ $latest_version->published_at->normalize }}</small></h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-custom">
                                            <div class="card-header bg-danger">
                                                <h3 class="card-title text-white"><i class="fas fa-times-circle text-white fa-lg"></i>&nbsp; Non Commencer</h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="timeline timeline-justified timeline-4">
                                                    <div class="timeline-bar"></div>
                                                    <div class="timeline-items">
                                                        @foreach(collect($latest_version->tasks)->where('etat', 0) as $task)
                                                        <div class="timeline-item">
                                                            <div class="timeline-badge">
                                                                <div class="bg-danger"></div>
                                                            </div>

                                                            <div class="timeline-label">
                                                                <span class="text-primary font-weight-bold">{{ $task->started_at->human }}</span>
                                                            </div>

                                                            <div class="timeline-content">
                                                                <strong class="mb-5">{{ $task->name }}</strong>
                                                                <p>{!! $task->description !!}</p>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-custom">
                                            <div class="card-header bg-warning">
                                                <h3 class="card-title text-white"><i class="fas fa-refresh text-white fa-lg"></i>&nbsp; En cours...</h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="timeline timeline-justified timeline-4">
                                                    <div class="timeline-bar"></div>
                                                    <div class="timeline-items">
                                                        @foreach(collect($latest_version->tasks)->where('etat', 1) as $task)
                                                        <div class="timeline-item">
                                                            <div class="timeline-badge">
                                                                <div class="bg-danger"></div>
                                                            </div>

                                                            <div class="timeline-label">
                                                                <span class="text-primary font-weight-bold">{{ $task->started_at->human }}</span>
                                                            </div>

                                                            <div class="timeline-content">
                                                                <strong class="mb-5">{{ $task->name }}</strong>
                                                                <p>{!! $task->description !!}</p>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-custom">
                                            <div class="card-header bg-success">
                                                <h3 class="card-title text-white"><i class="fas fa-check-circle text-white fa-lg"></i>&nbsp; Terminer</h3>
                                            </div>
                                            <div class="card-body">
                                                <div class="timeline timeline-justified timeline-4">
                                                    <div class="timeline-bar"></div>
                                                    <div class="timeline-items">
                                                        @foreach(collect($latest_version->tasks)->where('etat', 2) as $task)
                                                        <div class="timeline-item">
                                                            <div class="timeline-badge">
                                                                <div class="bg-danger"></div>
                                                            </div>

                                                            <div class="timeline-label">
                                                                <span class="text-primary font-weight-bold">{{ $task->finished_at->human }}</span>
                                                            </div>

                                                            <div class="timeline-content">
                                                                <strong class="mb-5">{{ $task->name }}</strong>
                                                                <p>{!! $task->description !!}</p>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="download" role="tabpanel">
            <div class="container">
                <div class="card card-custom">
                    <div class="card-header">
                        <h3 class="card-title">Téléchargement</h3>
                    </div>
                    <div class="card-body">
                        <div class="timeline timeline-3">
                            <div class="timeline-items">
                                @foreach($downloads as $download)
                                <div class="timeline-item">
                                    <div class="timeline-media">
                                        @if($download->alpha == 1) <span class="font-size-h5 font-weight-bold text-danger" data-toggle="tooltip" title="Alpha">A</span> @endif
                                        @if($download->beta == 1) <span class="font-size-h5 font-weight-bold text-warning" data-toggle="tooltip" title="Beta">B</span> @endif
                                        @if($download->release == 1) <span class="font-size-h5 font-weight-bold text-success" data-toggle="tooltip" title="Release">R</span> @endif
                                    </div>
                                    <div class="timeline-content">
                                        <div class="d-flex align-items-center justify-content-between mb-3">
                                            <div class="mr-2">
                                                <a href="#" class="text-dark-75 text-hover-primary font-weight-bold">V.{{ $download->version }}:{{ $download->build }}</a>
                                                <span class="text-muted ml-2">
                                                    @if(\Carbon\Carbon::createFromTimestamp(strtotime($download->published_at->format)) >= now()->format('00:00:00') && \Carbon\Carbon::createFromTimestamp(strtotime($download->published_at->format)) <= now()->format('23:59:59'))
                                                        {{ \Carbon\Carbon::createFromTimestamp(strtotime($download->published_at->format))->diffForHumans() }}
                                                    @else
                                                        {{ \Carbon\Carbon::createFromTimestamp(strtotime($download->published_at->format))->format('d/m/Y à H:i') }}
                                                    @endif
                                                </span>
                                            </div>
                                            <a href="{{ $download->link }}" class="btn btn-sm btn-primary"><i class="fas fa-download"></i> Télécharger</a>
                                        </div>
                                        <p class="p-0">
                                            {{ $download->note }}
                                        </p>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="session" role="tabpanel">
            <div class="container">
                <div class="card card-custom">
                    <div class="card-header">
                        <h3 class="card-title">Sessions</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach($sessions as $session)
                            <div class="col-md-4">
                                <div class="card card-custom shadow">
                                    <div class="card-header bgi-position-center bgi-size-cover bgi-no-repeat d-flex flex-column align-items-start justify-content-start" style="background: url('{{ $session->image }}'); height: 150px;">
                                        <div class="p-4 flex-grow-1">
                                            <h4 class="text-white font-weight-bold">{{ $session->name }}</h4>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p class="mb-3">{!! $session->short_content !!}</p>
                                        <strong>Kuid: </strong>{{ $session->kuid }}
                                    </div>
                                    <div class="card-footer text-right">
                                        <a href="#description{{ $session->id }}" data-toggle="modal" class="btn btn-icon btn-clean"><i class="fas fa-eye text-hover-primary"></i> </a>
                                        <a href="{{ $session->link }}" class="btn btn-icon btn-clean"><i class="fas fa-download text-hover-primary"></i> </a>
                                    </div>
                                </div>
                            </div>
                                <div class="modal fade" id="description{{ $session->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{ $session->name }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <i aria-hidden="true" class="ki ki-close"></i>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! $session->content !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="https://vjs.zencdn.net/7.8.4/video.js"></script>
    <script src="{{ asset('plugins/custom/lightbox/js/lightbox.js') }}"></script>
    <script src="{{ asset('js/route/show.js') }}" type="text/javascript"></script>
@endsection
