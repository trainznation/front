{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="container">
        <div class="row" id="routeElement"></div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/route/index.js') }}" type="text/javascript"></script>
@endsection
