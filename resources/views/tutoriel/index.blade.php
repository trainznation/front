{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')
    <div class="container-fluid">
        <div class="card card-custom">
            <div class="card-body">
                <ul class="nav nav-tabs nav-tabs-line justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#all">
                            <span class="nav-icon"><i class="fas fa-home"></i></span>
                            <span class="nav-text">Tous</span>
                        </a>
                    </li>
                    @foreach($categories->data as $category)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#{{ $category->slug }}">
                            <span class="nav-icon"><img src="{{ $category->image }}" alt="" style="width: 24px; height: 24px;"></span>
                            <span class="nav-text">{{ $category->name }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
                <div class="tab-content mt-5">
                    <div class="tab-pane fade show active" id="all" role="tabpanel">
                        <h2 class="card-title">Liste des Tutoriels</h2>
                        <div class="row">
                            @foreach($tutoriels->data as $tutoriel)
                                <div class="col-md-4">
                                    <a class="item" href="{{ route('Tutoriel.show', $tutoriel->id) }}">
                                        <div class="card card-custom gutter-b">
                                            <div class="card-header bgi-no-repeat bgi-position-center bgi-size-cover ribbon ribbon-top ribbon-ver" style="background-image: url('{{ $tutoriel->image }}'); height: 320px;">
                                                @if($tutoriel->published_at->format > now())
                                                    <div class="ribbon-target bg-warning" data-toggle="tooltip" title="Bientôt disponible" style="top: -2px; right: 20px;">
                                                        {{ $tutoriel->published_at->normalize }}
                                                    </div>
                                                @else
                                                    <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                                        {{ $tutoriel->published_at->normalize }}
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-3"><a href="" class="font-weight-bold font-size-h3 text-dark">{{ $tutoriel->title }}</a></div>
                                                {{ $tutoriel->short_content }}
                                            </div>
                                            <div class="card-footer text-muted row">
                                                <div class="col-md-6">
                                                    <i class="fas fa-clock"></i> {{ $tutoriel->time }}
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @foreach($categories->data as $category)
                        <div class="tab-pane fade" id="{{ $category->slug }}" role="tabpanel">
                            <div class="card-title">
                                <span class="card-icon"><img src="{{ $category->image }}" alt="" style="width: 24px; height: 24px;"/> </span>
                                <h3 class="card-title">{{ $category->name }}</h3>
                            </div>

                            <div class="row">
                                @foreach($category->tutoriels as $tutoriel)
                                    <div class="col-md-4">
                                        <a class="item" href="{{ route('Tutoriel.show', $tutoriel->id) }}">
                                            <div class="card card-custom gutter-b">
                                                <div class="card-header bgi-no-repeat bgi-position-center bgi-size-cover ribbon ribbon-top ribbon-ver" style="background-image: url('{{ $tutoriel->image }}'); height: 320px;">
                                                    @if($tutoriel->published_at->format > now())
                                                        <div class="ribbon-target bg-warning" data-toggle="tooltip" title="Bientôt disponible" style="top: -2px; right: 20px;">
                                                            {{ $tutoriel->published_at->normalize }}
                                                        </div>
                                                    @else
                                                        <div class="ribbon-target bg-danger" style="top: -2px; right: 20px;">
                                                            {{ $tutoriel->published_at->normalize }}
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="card-body">
                                                    <div class="mb-3"><a href="" class="font-weight-bold font-size-h3 text-dark">{{ $tutoriel->title }}</a></div>
                                                    {{ $tutoriel->short_content }}
                                                </div>
                                                <div class="card-footer text-muted row">
                                                    <div class="col-md-6">
                                                        <i class="fas fa-clock"></i> {{ $tutoriel->time }}
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/tutoriel/index.js') }}" type="text/javascript"></script>
@endsection
