{{-- Extends layout --}}
@extends('layout.default')

@section("page_toolbar")
    <button class="btn btn-xs btn-clean" onclick="window.history.back(-1)"><i class="fa fa-arrow-circle-left"></i> Retour à la liste des tutoriel</button>
@endsection

{{-- Content --}}
@section('content')
    <div class="container-fluid">
        <div class="card card-custom">
            @if($tutoriel->published_at->format > now())
                @if(session()->get('user')->account->premium == 1)
                    <div class="card-header bgi-no-repeat bgi-size-cover d-flex flex-column align-items-center justify-content-center" style="background-image: url('{{ $tutoriel->image }}')">
                        <video class="video-js" controls preload="auto" data-setup="{}">
                            <source src="{{ $tutoriel->video }}" type="video/mp4">
                        </video>
                        <div class="mt-lg-5 mb-lg-5 text-center text-white">
                            <h3 class="p-1 display-3 font-weight-bold">{{ $tutoriel->title }}</h3>
                            <h3 class="p-1 text-muted">Tutoriel Video <a href="/tutoriel" class="btn-link">{{ $tutoriel->category->name }}</a></h3>
                        </div>
                        <div class="mt-lg-5 mb-lg-5 text-center">
                            @if(session()->get('user')->account->premium == 1)
                                <button id="downloadVideo" class="btn btn-lg btn-primary"><i class="fas fa-play-circle"></i> Télécharger la vidéo</button>
                                @if($tutoriel->source == 1)
                                    <button id="downloadSource" class="btn btn-lg btn-primary"><i class="fas fa-download"></i> Télécharger les sources</button>
                                @endif
                            @else
                                <button id="downloadVideo" class="btn btn-lg btn-primary" disabled data-toggle="tooltip" title="Vous devez être premium"><i class="fas fa-play-circle"></i> Télécharger la vidéo</button>
                                @if($tutoriel->source == 1)
                                    <button id="downloadSource" class="btn btn-lg btn-primary" disabled data-toggle="tooltip" title="Vous devez être premium"><i class="fas fa-download"></i> Télécharger les sources</button>
                                @endif
                            @endif
                        </div>
                    </div>
                @else
                    <div class="card-header bgi-no-repeat bgi-size-cover d-flex flex-column align-items-center justify-content-center" style="background-image: url('{{ $tutoriel->image }}')">
                        <div class="mt-5 text-white text-center align-middle" style="width: 100%; background-color: rgba(62,59,60,0.73);">
                            <div class="font-weight-bolder display-2">Tutoriel disponible dans</div>
                            <div class="display-3" style="height: 200px;" id="countdown" data-time="{{ \Carbon\Carbon::createFromTimestamp(strtotime($tutoriel->published_at->format)) }}"></div>
                        </div>
                        <div class="mt-lg-5 mb-lg-5 text-center text-white">
                            <h3 class="p-1 display-3 font-weight-bold">{{ $tutoriel->title }}</h3>
                            <h3 class="p-1 text-muted">Tutoriel Video <a href="/tutoriel" class="btn-link">{{ $tutoriel->category->name }}</a></h3>
                        </div>
                        <div class="mt-lg-5 mb-lg-5 text-center">
                            @if(session()->get('user')->account->premium == 1)
                                <button id="downloadVideo" class="btn btn-lg btn-primary"><i class="fas fa-play-circle"></i> Télécharger la vidéo</button>
                                @if($tutoriel->source == 1)
                                    <button id="downloadSource" class="btn btn-lg btn-primary"><i class="fas fa-download"></i> Télécharger les sources</button>
                                @endif
                            @else
                                <button id="downloadVideo" class="btn btn-lg btn-primary" disabled data-toggle="tooltip" title="Vous devez être premium"><i class="fas fa-play-circle"></i> Télécharger la vidéo</button>
                                @if($tutoriel->source == 1)
                                    <button id="downloadSource" class="btn btn-lg btn-primary" disabled data-toggle="tooltip" title="Vous devez être premium"><i class="fas fa-download"></i> Télécharger les sources</button>
                                @endif
                            @endif
                        </div>
                    </div>
                @endif
            @else
                <div class="card-header bgi-no-repeat bgi-size-cover d-flex flex-column align-items-center justify-content-center" style="background-image: url('{{ $tutoriel->image }}')">
                    <video class="video-js" controls preload="auto" data-setup="{}">
                        <source src="{{ $tutoriel->video }}" type="video/mp4">
                    </video>
                    <div class="mt-lg-5 mb-lg-5 text-center text-white">
                        <h3 class="p-1 display-3 font-weight-bold">{{ $tutoriel->title }}</h3>
                        <h3 class="p-1 text-muted">Tutoriel Video <a href="/tutoriel" class="btn-link">{{ $tutoriel->category->name }}</a></h3>
                    </div>
                    <div class="mt-lg-5 mb-lg-5 text-center">
                        @if(session()->get('user')->account->premium == 1)
                            <button id="downloadVideo" class="btn btn-lg btn-primary"><i class="fas fa-play-circle"></i> Télécharger la vidéo</button>
                            @if($tutoriel->source == 1)
                                <button id="downloadSource" class="btn btn-lg btn-primary"><i class="fas fa-download"></i> Télécharger les sources</button>
                            @endif
                        @else
                            <button id="downloadVideo" class="btn btn-lg btn-primary" disabled data-toggle="tooltip" title="Vous devez être premium"><i class="fas fa-play-circle"></i> Télécharger la vidéo</button>
                            @if($tutoriel->source == 1)
                                <button id="downloadSource" class="btn btn-lg btn-primary" disabled data-toggle="tooltip" title="Vous devez être premium"><i class="fas fa-download"></i> Télécharger les sources</button>
                            @endif
                        @endif
                    </div>
                </div>
            @endif
            <div class="card-body">
                <div class="container">
                    <div class="row text-muted">
                        <div class="col-md-6">Tutoriel / {{ $tutoriel->category->name }} / {{ $tutoriel->title }}</div>
                        <div class="col-md-6 text-right">{{ $tutoriel->published_at->human }}</div>
                    </div>
                    <div class="row" style="margin-top: 75px;">
                        <div class="col-md-1">
                            <a href=""><i class="socicon-facebook"></i> </a><br>
                            <a href=""><i class="socicon-twitter"></i> </a>
                        </div>
                        <div class="col-md-9">
                            <div id="tutorielContent" class="font-size-lg" data-content="{{ $tutoriel->content }}"></div>
                        </div>
                        <div class="col-md-2">
                            <div class="d-flex align-items-center mb-10">
                                <!--begin::Symbol-->
                                <div class="symbol symbol-40 symbol-light-white symbol-circle mr-5">
                                    <div class="symbol-label">
                                        <img src="{{ asset('media/logos/logo-letter-1.png') }}" class="h-75 align-self-end" alt="">
                                    </div>
                                </div>
                                <!--end::Symbol-->
                                <!--begin::Text-->
                                <div class="d-flex flex-column font-weight-bold">
                                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Auteur</a>
                                    <span class="text-muted">Trainznation</span>
                                </div>
                                <!--end::Text-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/tutoriel/show.js') }}" type="text/javascript"></script>
@endsection
