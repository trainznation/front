<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "home", "uses" => "PagesController@index"]);

include('Router/auth.php');
include('Router/blog.php');
include('Router/route.php');
include('Router/tutoriel.php');
include('Router/download.php');
include('Router/account.php');
include('Router/cart.php');
include('Router/checkout.php');
include('Router/wiki.php');

Route::post('/stripe', 'StripeWebhooksController');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
