<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "account", "namespace" => "Account"], function () {
    Route::group(["prefix" => "profil"], function () {
        Route::get('', ["as" => "Account.profil", "uses" => "ProfilController@profil"]);
        Route::put('/editing', ["as" => "Account.editing", "uses" => "ProfilController@editing"]);
        Route::get('/delete', ["as" => "Account.delete", "uses" => "ProfilController@delete"]);
        Route::put('/changePassword', ["as" => "Account.changePassword", "uses" => "ProfilController@changepassword"]);
    });

    Route::group(["prefix" => "notification"], function () {
        Route::get('/', ["as" => "Notification.index", "uses" => "NotificationController@index"]);
        Route::get('{notif_id}/read', ["as" => "Notification.read", "uses" => "NotificationController@read"]);
        Route::get('readAll', ["as" => "Notification.readAll", "uses" => "NotificationController@readAll"]);
    });

    Route::group(["prefix" => "badge"], function () {
        Route::get('/', ["as" => "Badge.index", "uses" => "BadgeController@index"]);
    });

    Route::group(["prefix" => "social"], function () {
        Route::get('{provider}', ["as" => "Account.SocialProviding", "uses" => "SocialController@SocialProviding"]);
    });

    Route::group(["prefix" => "order"], function () {
        Route::get('/', ["as" => "Order.index", "uses" => "OrderController@index"]);
        Route::get('{order_id}', ["as" => "Order.show", "uses" => "OrderController@show"]);
    });

    Route::group(["prefix" => "download"], function () {
        Route::get('/', ["as" => "Account.Download.index", "uses" => "DownloadController@index"]);
    });

    Route::group(["prefix" => "premium"], function () {
        Route::get('/', ["as" => "Account.Premium.index", "uses" => "PremiumController@index"]);
        Route::get('/subscribe', ["as" => "Account.Premium.subscribe", "uses" => "PremiumController@subscribe"]);
        Route::post('/subscribe', ["as" => "Account.Premium.activate", "uses" => "SubscribeController@subscribe"]);
        Route::get('/unsubscribe', ["as" => "Account.Premium.desactivate", "uses" => "PremiumController@desactivate"]);
    });

    Route::group(["prefix" => "support"], function () {
        Route::group(["prefix" => "ticket"], function () {
            Route::post('/', ["as" => "Support.Ticket.store", "uses" => "TicketController@store"]);
        });
    });
});
