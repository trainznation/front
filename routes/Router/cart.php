<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "cart", "namespace" => "Cart"], function () {
    Route::get('/', ["as" => "Cart.index", "uses" => "CartController@index"]);
    Route::post('/', ["as" => "Cart.store", "uses" => "CartController@store"]);
    Route::delete('{id}', ["as" => "Cart.destroy", "uses" => "CartController@destroy"]);
});
