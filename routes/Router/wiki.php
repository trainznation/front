<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "wiki", "namespace" => "Wiki"], function () {
    Route::get('/', ["as" => "Wiki.index", "uses" => "WikiController@index"]);
    Route::get('/sector/{category_id}', ["as" => "Wiki.sector", "uses" => "WikiController@sector"]);
    Route::get('/sector/{sector_id}/list', ["as" => "Wiki.articles", "uses" => "WikiController@articles"]);
    Route::get('{wiki_id}', ["as" => "Wiki.show", "uses" => "WikiController@show"]);
});
