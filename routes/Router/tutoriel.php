<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "tutoriel", "namespace" => "Tutoriel"], function () {
    Route::get('/', ["as" => "Tutoriel.index", "uses" => "TutorielController@index"]);
    Route::get('{tutoriel_id}', ["as" => "Tutoriel.show", "uses" => "TutorielController@show"]);
});
