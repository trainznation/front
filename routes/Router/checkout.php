<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "checkout", "namespace" => "Checkout"], function () {
    Route::get('create', ["as" => "Checkout.create", "uses" => "CheckoutController@create"]);
    Route::post('create', ["as" => "Checkout.store", "uses" => "CheckoutController@store"]);
    Route::get('payment', ["as" => "Checkout.payment", "uses" => "CheckoutController@payment"]);
    Route::post('payment', "PaymentController@store");
});
