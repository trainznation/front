<?php

use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "auth", "namespace" => "Auth"], function () {
    Route::get('login', ["as" => "login", "uses" => "LoginController@index"]);
    Route::post('login', ["as" => "login.post", "uses" => "LoginController@login"]);
    Route::get('logout', ["as" => "logout", "uses" => "LoginController@logout"]);

    Route::get('register', ["as" => "register", "uses" => "RegisterController@index"]);
    Route::post('register', ["as" => "register.post", "uses" => "RegisterController@register"]);

    Route::get('password/reset/request', 'PasswordController@resetRequest')->name('password.request');
    Route::post('password/reset/request', 'PasswordController@postResetRequest')->name('password.postRrequest');

    Route::get('password/check', 'PasswordController@checkToken')->name('password.checkToken');
    Route::post('password/check', 'PasswordController@postReset')->name('password.postReset');

    Route::get('/me', 'UserController@me');
    Route::get('/notif/{user_id}/{notif_id}', 'UserController@readNotif');
});
