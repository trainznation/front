<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "route", "namespace" => "Route"], function () {
    Route::get('/', ["as" => "Route.index", "uses" => "RouteController@index"]);
    Route::get('{route_id}', ["as" => "Route.show", "uses" => "RouteController@show"]);
});
