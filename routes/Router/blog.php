<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "blog", "namespace" => "Blog"], function () {
    Route::get('/', ["as" => "Blog.index", "uses" => "BlogController@index"]);
    Route::get('{blog_id}', ["as" => "Blog.show", "uses" => "BlogController@show"]);
});
