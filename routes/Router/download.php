<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "download", "namespace" => "Download"], function () {
    Route::get('/', ["as" => "Download.index", "uses" => "DownloadController@index"]);
    Route::get('{asset_id}', ["as" => "Download.show", "uses" => "DownloadController@show"]);
});
