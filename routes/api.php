<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["prefix" => "blog", "namespace" => "Api\Blog"], function () {
    Route::get('/latest', 'BlogController@latest');
    Route::get('/list', 'BlogController@list');
    Route::post('{blog_id}/comment', 'BlogController@postComment');
    Route::delete('{blog_id}/comment/{comment_id}', 'BlogController@deleteComment');
});

Route::group(["prefix" => "route", "namespace" => "Api\Route"], function () {
    Route::get('/list', 'RouteController@list');

    Route::get('{route_id}/gallery/category/{category_id}', 'RouteController@getGallery');
});

Route::group(["prefix" => "asset", "namespace" => "Api\Asset"], function () {
    Route::get('/latest', 'AssetController@latest');
});

Route::group(["prefix" => "tutoriel", "namespace" => "Api\Tutoriel"], function () {
    Route::get('/latest', 'TutorielController@latest');
});

Route::group(["prefix" => "wiki", "namespace" => "Api\Wiki"], function () {
    Route::post('search', 'WikiController@search');
});

Route::group(["prefix" => "core", "namespace" => "Api\Core"], function () {
    Route::get('announcement/list', 'CoreController@listAnnouncement');
});
