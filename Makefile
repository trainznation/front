sy := php artisan
sp := php
ipAddr := 192.168.0.18
user := root
path := /www/wwwroot/
namePath := admin.trainznation.tk/current
repository := https://gitlab.com/trainznation/admin-trainznation.git
ssh := ssh $(user)@$(ipAddr)

.DEFAULT_GOAL := help
.PHONY: help
help: ## Affiche cette aide
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: clear
clear: ## Nettoie toues les caches du système
	$(sy) down
	$(sy) clear-compiled
	$(sy) optimize
	$(sy) cache:clear
	$(sy) config:clear
	$(sy) event:clear
	$(sy) route:clear
	$(sy) view:clear
	$(sy) up

.PHONY: coverage
test: ## Effectue un test par artisan avec coverage
	$(sy) test --coverage-html=coverage

